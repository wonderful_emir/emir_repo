/**********************************************************************************/
/*!
\file   	entity.hh
\language 	C++
\author 	Emir Khanseitov
\par    	email: emir.k@digipen.edu
\par    	digiPen login: emir.k
\par    	Course: CS225
\par    	Assignment #1 - Chess
\date   	28/09/2018
\updated 	12/10/2018
\brief  
    Contains Entity class with all the necessary functions declared, 
	also has an exception class
        

\par Hours spent on this assignment: 
    30-40 hours, very desorganized hours


\par Specific portions that gave you the most trouble: 
    - Understanding concepts behind entity-component systems 
	- Managing the time around the assignment
	- Going straight on without stopping programming while coding the assignments
  
*/
/**********************************************************************************/

#pragma once

#include "Component.hh"
#include <vector>


// Tests go through cs225 namespace
namespace cs225 {
	
	
	// ENTITY CLASS
	class Entity
	{
	public:
		
/*!****************************************************************************
 \fn	 Entity()
 
 \brief	 Constructor for initialization of m_components_counter

 \param  void

 \return void
******************************************************************************/
		Entity();
		
/*!****************************************************************************
 \fn	 Entity(const Entity& other_entit)
 
 \brief	 Copy constructor for deep copy of the Entity

 \param  other_entit
const Entity&

 \return void
******************************************************************************/
		Entity(const Entity& other_entit);
		
/*!****************************************************************************
 \fn	 ~Entity()
 
 \brief	 Destructor re-definition for proper de-allocation

 \param  void

 \return void
******************************************************************************/
		~Entity();
		
/*!****************************************************************************
 \fn	 component_count() const
 
 \brief	 Function to see how many components there are

 \param  void

 \return unsigned int
******************************************************************************/
		unsigned int component_count() const;
		
/*!****************************************************************************
 \fn	 attach(Component* added_comp)
 
 \brief	 Function that attaches Components to the entity by ptr

 \param  added_comp
 Component*

 \return void
******************************************************************************/
		void attach(Component* added_comp);
		
/*!****************************************************************************
 \fn	 attach(Component* added_comp)
 
 \brief	 Overload of the function that attaches comps by reference

 \param  ref_comp
 const Component&

 \return void
******************************************************************************/
		void attach( const Component& ref_comp );
		
/*!****************************************************************************
 \fn	 operator[](unsigned int index) const
 
 \brief	 Operator overload for [] to access a particular component

 \param  index
 unsigned int

 \return Component&
******************************************************************************/
		Component& operator[](unsigned int index) const;
		
/*!****************************************************************************
 \fn	 get_component_type()
 
 \brief	 Helper method to find a concrete component in entity

 \param  void

 \return T*
******************************************************************************/
		template <typename T>
		T* get_component_type()
		{
			// Store size of the vector to not calculate it every iteration
			unsigned int vector_size = attached_components.size();
			
			// Loop until you find a component of the same type
			for(unsigned int i = 0; i < vector_size; ++i)
				if(dynamic_cast<T*>(attached_components[i]) != NULL)
					return dynamic_cast<T*>(attached_components[i]);
			
			// Unless we didn't find a concrete component, then return NULL
			return NULL;
		}
		
	private:
		// how many components there are on the entity
		unsigned int m_components_counter;
		
		// vector of components attached to the entity
		std::vector<Component*> attached_components;
		
	};
	
	
	// EXCEPTION CLASS
	class InvalidIndex
	{
	public:
/*!****************************************************************************
 \fn	 InvalidIndex(unsigned int index)
 
 \brief	 Non-default Constructor for initialization of wrong_value 

 \param  void

 \return void
******************************************************************************/
		InvalidIndex(unsigned int index);
	
/*!****************************************************************************
 \fn	 value() const
 
 \brief	 Getter function for value in InvalidIndex 

 \param  void

 \return unsigned int
******************************************************************************/
		unsigned int value() const;
		
	private:
		unsigned int wrong_value;
	};

} // namespace cs225