/**********************************************************************************/
/*!
\file   	chess_piece.hh
\language 	C++
\author 	Emir Khanseitov
\par    	email: emir.k@digipen.edu
\par    	digiPen login: emir.k
\par    	Course: CS225
\par    	Assignment #1 - Chess
\date   	28/09/2018
\updated 	12/10/2018
\brief  
    Contains classes such as Piece that is inherited from entity, and component
	classes that would be used to attach onto entity such as Position, PieceVisuals, 
	PawnMovement, interface for movements PieceMovement and other Movements
        

\par Hours spent on this assignment: 
    30-40 hours, very desorganized hours


\par Specific portions that gave you the most trouble: 
    - Understanding concepts behind entity-component systems 
	- Managing the time around the assignment
	- Going straight on without stopping programming while coding the assignments
  
*/
/**********************************************************************************/

#pragma once
#include "entity.hh"
#include "chess_board.hh"

#define WHITE 1
#define BLACK 0


namespace cs225 {	namespace chess	{

	// PIECE ENTITY CLASS
	class Piece : public Entity
	{
	public:
/*!****************************************************************************
 \fn	 is_valid_move(BoardPosition possible_move)
 
 \brief	 Function to know if it's valid move

 \param  possible_move
 BoardPosition

 \return bool
******************************************************************************/
		bool is_valid_move(BoardPosition possible_move);
	};
	
	
	
	// POSITION COMPONENT CLASS
	class Position : public Component
	{
	public:
/*!****************************************************************************
 \fn	 Position(int Row, char Column)
 
 \brief	 Non-default constructor for initializing Row and Column

 \param  Row
int

 \param  Column
char

 \return void
******************************************************************************/
		Position(int Row, char Column);
		
/*!****************************************************************************
 \fn	 Position(int Row, char Column)
 
 \brief	 Copy constructor for initializing Row and Column with BoardPosition

 \param  pos
BoardPosition

 \return void
******************************************************************************/
		Position(BoardPosition pos);
		
/*!****************************************************************************
 \fn	 ~Position()
 
 \brief	 Virtual destructor

 \param void

 \return void
******************************************************************************/
		virtual ~Position();

/*!****************************************************************************
 \fn	 type() const 
 
 \brief	 virtual function to get the type of the Component

 \param  void

 \return const char*
******************************************************************************/
		virtual const char* type() const;

/*!****************************************************************************
 \fn	 load( std::istream& is )
 
 \brief	 load function for serialization

 \param is
std::istream&

 \return void
******************************************************************************/
		virtual void load( std::istream& is );
		
/*!****************************************************************************
 \fn	 save( std::ostream& os ) const 
 
 \brief	 save function to store in a text file

 \param os
 std::ostream&

 \return void
******************************************************************************/
		virtual void save( std::ostream& os ) const;

/*!****************************************************************************
 \fn	 clone() const 
 
 \brief	 virtual function to clone itself

 \param  void

 \return Component*
******************************************************************************/
		virtual Component* clone() const;
		
/*!****************************************************************************
 \fn	 compare_to( const IComparable& ) const 
 
 \brief	 function to compare number of components (I suppose)

 \param  ...
const IComparable& 

 \return int
******************************************************************************/
		virtual int compare_to(const IComparable&) const;
		
/*!****************************************************************************
 \fn	 operator== (BoardPosition the_board) const
 
 \brief	 Operator overload to check if positions are same

 \param  the_board
BoardPosition

 \return bool
******************************************************************************/
		bool operator== (BoardPosition the_board) const;
		
/*!****************************************************************************
 \fn	 Get_row() const 
 
 \brief	 Function Getter to get Row Number

 \param  void

 \return int
******************************************************************************/
		int Get_row() const;
		
/*!****************************************************************************
 \fn	 Get_column() const 
 
 \brief	 Function Getter to get Row Letter

 \param  void

 \return char
******************************************************************************/
		char Get_column() const;
		
		
	private:
		BoardPosition m_pos_on_board;
	};
	
	
	// PIECEVISUALS COMPONENT CLASS
	class PieceVisuals : public Component
	{
	public:
/*!****************************************************************************
 \fn	 PieceVisuals(char The_piece = 'p', int which_color = 1)
 
 \brief	 Default constructor for initializing Letter and color of piece

 \param  The_piece
 char
 
 \param	 which_color
 int

 \return void
******************************************************************************/
		PieceVisuals(char The_piece = 'p', int which_color = 1);
		
/*!****************************************************************************
 \fn	 ~PieceVisuals()
 
 \brief	 Virtual destructor

 \param void

 \return void
******************************************************************************/
		virtual ~PieceVisuals();
		
/*!****************************************************************************
 \fn	 type() const 
 
 \brief	 virtual function to get the type of the Component

 \param  void

 \return const char*
******************************************************************************/
		virtual const char* type() const;

/*!****************************************************************************
 \fn	 load( std::istream& is )
 
 \brief	 load function for serialization

 \param is
std::istream&

 \return void
******************************************************************************/
		virtual void load( std::istream& is );
		
/*!****************************************************************************
 \fn	 save( std::ostream& os ) const 
 
 \brief	 save function to store in a text file

 \param os
 std::ostream&

 \return void
******************************************************************************/
		virtual void save( std::ostream& os ) const;

/*!****************************************************************************
 \fn	 clone() const 
 
 \brief	 virtual function to clone itself

 \param  void

 \return Component*
******************************************************************************/
		virtual Component* clone() const;
		
/*!****************************************************************************
 \fn	 compare_to( const IComparable& ) const 
 
 \brief	 function to compare number of components (I suppose)

 \param  ...
const IComparable& 

 \return int
******************************************************************************/
		virtual int compare_to(const IComparable&) const;

/*!****************************************************************************
 \fn	 shape() const 
 
 \brief	 Function Getter to get shape of the piece

 \param  void

 \return char
******************************************************************************/
		char shape() const;
		
/*!****************************************************************************
 \fn	 color() const 
 
 \brief	 Function Getter to get Color of the piece

 \param  void

 \return int
******************************************************************************/
		int color() const;
		
	private:
		char m_which_piece;
		int m_piece_color;
	};
	
	
	
	// PIECEMOVEMENT COMPONENT CLASS
	class PieceMovement	: public Component
	{
	public:

/*!****************************************************************************
 \fn	 ~PieceMovement()
 
 \brief	 Virtual destructor

 \param void

 \return void
******************************************************************************/
		virtual ~PieceMovement() {}
		
/*!****************************************************************************
 \fn	 is_valid_move(BoardPosition possible_move) const 
 
 \brief	 Pure virtual function to check if its a valid move

 \param  possible_move
BoardPosition

 \return bool
******************************************************************************/
		virtual bool is_valid_move(BoardPosition possible_move) const = 0;
	};
	
	
	// PAWNMOVEMENT COMPONENT CLASS
	class PawnMovement	: public PieceMovement
	{
	public:
/*!****************************************************************************
 \fn	 ~PawnMovement()
 
 \brief	 Virtual destructor

 \param void

 \return void
******************************************************************************/
		virtual ~PawnMovement();
		
/*!****************************************************************************
 \fn	 type() const 
 
 \brief	 virtual function to get the type of the Component

 \param  void

 \return const char*
******************************************************************************/
		virtual const char* type() const;

/*!****************************************************************************
 \fn	 load( std::istream& is )
 
 \brief	 load function for serialization

 \param is
std::istream&

 \return void
******************************************************************************/
		virtual void load( std::istream& is );
		
/*!****************************************************************************
 \fn	 save( std::ostream& os ) const 
 
 \brief	 save function to store in a text file

 \param os
 std::ostream&

 \return void
******************************************************************************/
		virtual void save( std::ostream& os ) const;

/*!****************************************************************************
 \fn	 clone() const 
 
 \brief	 virtual function to clone itself

 \param  void

 \return Component*
******************************************************************************/
		virtual Component* clone() const;
		
/*!****************************************************************************
 \fn	 compare_to( const IComparable& ) const 
 
 \brief	 function to compare number of components (I suppose)

 \param  ...
const IComparable& 

 \return int
******************************************************************************/
		virtual int compare_to(const IComparable&) const;
	
/*!****************************************************************************
 \fn	 is_valid_move(BoardPosition possible_move) const 
 
 \brief	 Virtual function to check if its a valid move

 \param  possible_move
BoardPosition

 \return bool
******************************************************************************/
		virtual bool is_valid_move(BoardPosition possible_move) const;
	};
	
	
	// BISHOPMOVEMENT COMPONENT CLASS
	class BishopMovement : public PieceMovement
	{
	public:
/*!****************************************************************************
 \fn	 ~BishopMovement()
 
 \brief	 Virtual destructor

 \param void

 \return void
******************************************************************************/
		virtual ~BishopMovement();
		
/*!****************************************************************************
 \fn	 type() const 
 
 \brief	 virtual function to get the type of the Component

 \param  void

 \return const char*
******************************************************************************/
		virtual const char* type() const;

/*!****************************************************************************
 \fn	 load( std::istream& is )
 
 \brief	 load function for serialization

 \param is
std::istream&

 \return void
******************************************************************************/
		virtual void load( std::istream& is );
		
/*!****************************************************************************
 \fn	 save( std::ostream& os ) const 
 
 \brief	 save function to store in a text file

 \param os
 std::ostream&

 \return void
******************************************************************************/
		virtual void save( std::ostream& os ) const;

/*!****************************************************************************
 \fn	 clone() const 
 
 \brief	 virtual function to clone itself

 \param  void

 \return Component*
******************************************************************************/
		virtual Component* clone() const;
		
/*!****************************************************************************
 \fn	 compare_to( const IComparable& ) const 
 
 \brief	 function to compare number of components (I suppose)

 \param  ...
const IComparable& 

 \return int
******************************************************************************/
		virtual int compare_to(const IComparable&) const;

/*!****************************************************************************
 \fn	 is_valid_move(BoardPosition possible_move) const 
 
 \brief	 Virtual function to check if its a valid move

 \param  possible_move
BoardPosition

 \return bool
******************************************************************************/
		virtual bool is_valid_move(BoardPosition possible_move) const;
	};
	
	class RookMovement : public PieceMovement
	{
	public:
/*!****************************************************************************
 \fn	 ~RookMovement()
 
 \brief	 Virtual destructor

 \param void

 \return void
******************************************************************************/
		virtual ~RookMovement();
		
/*!****************************************************************************
 \fn	 type() const 
 
 \brief	 virtual function to get the type of the Component

 \param  void

 \return const char*
******************************************************************************/
		virtual const char* type() const;

/*!****************************************************************************
 \fn	 load( std::istream& is )
 
 \brief	 load function for serialization

 \param is
std::istream&

 \return void
******************************************************************************/
		virtual void load( std::istream& is );
		
/*!****************************************************************************
 \fn	 save( std::ostream& os ) const 
 
 \brief	 save function to store in a text file

 \param os
 std::ostream&

 \return void
******************************************************************************/
		virtual void save( std::ostream& os ) const;

/*!****************************************************************************
 \fn	 clone() const 
 
 \brief	 virtual function to clone itself

 \param  void

 \return Component*
******************************************************************************/
		virtual Component* clone() const;
		
/*!****************************************************************************
 \fn	 compare_to( const IComparable& ) const 
 
 \brief	 function to compare number of components (I suppose)

 \param  ...
const IComparable& 

 \return int
******************************************************************************/
		virtual int compare_to(const IComparable&) const;

/*!****************************************************************************
 \fn	 is_valid_move(BoardPosition possible_move) const 
 
 \brief	 Virtual function to check if its a valid move

 \param  possible_move
BoardPosition

 \return bool
******************************************************************************/
		virtual bool is_valid_move(BoardPosition possible_move) const;
	};
	
	class KnightMovement : public PieceMovement
	{
	public:
/*!****************************************************************************
 \fn	 ~KnightMovement()
 
 \brief	 Virtual destructor

 \param void

 \return void
******************************************************************************/
		virtual ~KnightMovement();
		
/*!****************************************************************************
 \fn	 type() const 
 
 \brief	 virtual function to get the type of the Component

 \param  void

 \return const char*
******************************************************************************/
		virtual const char* type() const;

/*!****************************************************************************
 \fn	 load( std::istream& is )
 
 \brief	 load function for serialization

 \param is
std::istream&

 \return void
******************************************************************************/
		virtual void load( std::istream& is );
		
/*!****************************************************************************
 \fn	 save( std::ostream& os ) const 
 
 \brief	 save function to store in a text file

 \param os
 std::ostream&

 \return void
******************************************************************************/
		virtual void save( std::ostream& os ) const;

/*!****************************************************************************
 \fn	 clone() const 
 
 \brief	 virtual function to clone itself

 \param  void

 \return Component*
******************************************************************************/
		virtual Component* clone() const;
		
/*!****************************************************************************
 \fn	 compare_to( const IComparable& ) const 
 
 \brief	 function to compare number of components (I suppose)

 \param  ...
const IComparable& 

 \return int
******************************************************************************/
		virtual int compare_to(const IComparable&) const;

/*!****************************************************************************
 \fn	 is_valid_move(BoardPosition possible_move) const 
 
 \brief	 Virtual function to check if its a valid move

 \param  possible_move
BoardPosition

 \return bool
******************************************************************************/
		virtual bool is_valid_move(BoardPosition possible_move) const;
	};
	
	class KingMovement : public PieceMovement
	{
	public:
/*!****************************************************************************
 \fn	 ~KingMovement()
 
 \brief	 Virtual destructor

 \param void

 \return void
******************************************************************************/
		virtual ~KingMovement();
		
/*!****************************************************************************
 \fn	 type() const 
 
 \brief	 virtual function to get the type of the Component

 \param  void

 \return const char*
******************************************************************************/
		virtual const char* type() const;

/*!****************************************************************************
 \fn	 load( std::istream& is )
 
 \brief	 load function for serialization

 \param is
std::istream&

 \return void
******************************************************************************/
		virtual void load( std::istream& is );
		
/*!****************************************************************************
 \fn	 save( std::ostream& os ) const 
 
 \brief	 save function to store in a text file

 \param os
 std::ostream&

 \return void
******************************************************************************/
		virtual void save( std::ostream& os ) const;

/*!****************************************************************************
 \fn	 clone() const 
 
 \brief	 virtual function to clone itself

 \param  void

 \return Component*
******************************************************************************/
		virtual Component* clone() const;
		
/*!****************************************************************************
 \fn	 compare_to( const IComparable& ) const 
 
 \brief	 function to compare number of components (I suppose)

 \param  ...
const IComparable& 

 \return int
******************************************************************************/
		virtual int compare_to(const IComparable&) const;
		
/*!****************************************************************************
 \fn	 is_valid_move(BoardPosition possible_move) const 
 
 \brief	 Virtual function to check if its a valid move

 \param  possible_move
BoardPosition

 \return bool
******************************************************************************/
		virtual bool is_valid_move(BoardPosition possible_move) const;
	};
	
	class QueenMovement : public PieceMovement
	{
	public:
/*!****************************************************************************
 \fn	 ~QueenMovement()
 
 \brief	 Virtual destructor

 \param void

 \return void
******************************************************************************/
		virtual ~QueenMovement();
		
/*!****************************************************************************
 \fn	 type() const 
 
 \brief	 virtual function to get the type of the Component

 \param  void

 \return const char*
******************************************************************************/
		virtual const char* type() const;

/*!****************************************************************************
 \fn	 load( std::istream& is )
 
 \brief	 load function for serialization

 \param is
std::istream&

 \return void
******************************************************************************/
		virtual void load( std::istream& is );
		
/*!****************************************************************************
 \fn	 save( std::ostream& os ) const 
 
 \brief	 save function to store in a text file

 \param os
 std::ostream&

 \return void
******************************************************************************/
		virtual void save( std::ostream& os ) const;

/*!****************************************************************************
 \fn	 clone() const 
 
 \brief	 virtual function to clone itself

 \param  void

 \return Component*
******************************************************************************/
		virtual Component* clone() const;
		
/*!****************************************************************************
 \fn	 compare_to( const IComparable& ) const 
 
 \brief	 function to compare number of components (I suppose)

 \param  ...
const IComparable& 

 \return int
******************************************************************************/
		virtual int compare_to(const IComparable&) const;
		
/*!****************************************************************************
 \fn	 is_valid_move(BoardPosition possible_move) const 
 
 \brief	 Virtual function to check if its a valid move

 \param  possible_move
BoardPosition

 \return bool
******************************************************************************/
		virtual bool is_valid_move(BoardPosition possible_move) const;
	};


}	}	//namespace cs225::chess