/**********************************************************************************/
/*!
\file   	component.cc
\language 	C++
\author 	Emir Khanseitov
\par    	email: emir.k@digipen.edu
\par    	digiPen login: emir.k
\par    	Course: CS225
\par    	Assignment #1 - Chess
\date   	28/09/2018
\updated 	12/10/2018
\brief  
    Contains definitions for the functions that were not pure virtual in classes
	headers
        

\par Hours spent on this assignment: 
    30-40 hours, very desorganized hours


\par Specific portions that gave you the most trouble: 
    - Understanding concepts behind entity-component systems 
	- Managing the time around the assignment
	- Going straight on without stopping programming while coding the assignments
  
*/
/**********************************************************************************/

#include "component.hh"

#include "entity.hh"

namespace cs225{
	
	// Constructor to initialize the pointer
	Component::Component() : ptr_to_base(NULL) {}
	
	// Getter function to get the pointer for the base
	Entity* Component::owner() const	{ return ptr_to_base; }
	
	// Setter function to set the pointer to base
	void Component::SetOwner(Entity* base)	{ ptr_to_base = base; }
	
} // namespace cs225