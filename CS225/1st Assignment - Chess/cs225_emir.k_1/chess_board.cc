/**********************************************************************************/
/*!
\file   	chess_board.cc
\language 	C++
\author 	Emir Khanseitov
\par    	email: emir.k@digipen.edu
\par    	digiPen login: emir.k
\par    	Course: CS225
\par    	Assignment #1 - Chess
\date   	28/09/2018
\updated 	12/10/2018
\brief  
    Contains definitions of the functions in classes of BoardPosition and Color,
	as well the Exception class
        

\par Hours spent on this assignment: 
    30-40 hours, very desorganized hours


\par Specific portions that gave you the most trouble: 
    - Understanding concepts behind entity-component systems 
	- Managing the time around the assignment
	- Going straight on without stopping programming while coding the assignments
  
*/
/**********************************************************************************/

#include "chess_board.hh"

namespace cs225 { namespace chess{
	
	// Define which one is color white and which one is color black
	const int Color::white = 1;
	const int Color::black = 0;
	
	// Default Constructor
	BoardPosition::BoardPosition() : m_rows(1), m_columns('a') {}
	
	// Non-default constructor
	BoardPosition::BoardPosition(int Row, char Column) 
	{
		// Sanity check for going out of bounds
		if(Row > 8 || Column > 'h')
			throw(InvalidBoardPosition(Row, Column));
			
		// Apply the constructor for private members
		m_rows = Row;
		m_columns = Column;
		
		UpdateColor();
	}
	
	// Getter function to get number of rows
	int BoardPosition::row() const
	{
		// return number of rows
		return m_rows;
	}
	
	// Getter function to get number of columns
	char BoardPosition::column() const
	{
		// return number of columns
		return m_columns;
	}
	
	// Setter function for row and column
	void BoardPosition::set(int Row, char Column)
	{
		// Sanity check for going out of bounds, of chessboard
		if(Row > 8 || Column > 'h')
			throw(InvalidBoardPosition(Row, Column));
	
		// Set rows and column
		m_rows = Row;
		m_columns = Column;
		
		UpdateColor();
	}
	
	// Function for determining color of the cell in the board
	Color BoardPosition::color() const
	{
		return m_color;
	}
	
	void BoardPosition::UpdateColor()
	{
		// if even -> starts with White
		if(m_rows % 2 == 0)
		{
			// Then check only even ASCII letters, eg b -> 98 in ASCII
			
			// if even letters -> then Black Space
			if(m_columns % 2 == 0)
				m_color.color_setter(0);
			else
				m_color.color_setter(1);
		}
		// else is odd -> starts with Black space
		else
		{
			// Odd letters, eg 97 -> a, are black
			if(m_columns % 2 == 1)
				m_color.color_setter(0);
			else
				m_color.color_setter(1);
		}
	}
	
	int Color::color_getter() const	{	return which;	}
	
	void Color::color_setter(int other_color) {		which = other_color;	}
	
	// Operator overload for == check if white or black
	bool Color::operator== (const int White_color) const	{	return this->color_getter() == White_color;	}
	
	
	bool Color::operator== (Color which_color) const	{	return  color_getter() == which_color.color_getter();	}
	
	
	
	InvalidBoardPosition::InvalidBoardPosition(int inv_Row, char inv_Column) 
									: m_inv_rows(inv_Row), m_inv_columns(inv_Column) {}
									
	// Getter function to get number of rows
	int InvalidBoardPosition::row() const	{	return m_inv_rows;	}
	
	// Getter function to get number of columns
	char InvalidBoardPosition::column() const	{	return m_inv_columns;	}
	
} } // namespace cs225::chess