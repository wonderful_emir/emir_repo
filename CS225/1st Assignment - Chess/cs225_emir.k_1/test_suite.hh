/*! 
 * Test suite for the CS225 chess programming assignment.
 *
 * Contains the combined collection of tests for the assignment. 
 * Use this if you find it convenient to work with a single file that contains all the driver code.
 *
 * Modifications to this file are allowed only for development convenience (e.g. commenting out code
 * that fails to compile). This file will be used as provided during the grading process, so make sure
 * that your solution does not rely on any modification done on this file.
 *
 * Students are responsible for all code elements that live in the `cs225` namespace. These should 
 * be declared in the header files as hinted by the inclussion directives.
 *
 * If you want to disable a test, just comment it out. Bear in mind that the `TEST` macro makes it 
 * necessary to compile and run the test code, so both compilation and run time errors should
 * be taken into account.
 *
 * @author  Iker Silvano
 *
 */

#include "testing.hh" // testing framework (ASSERT_THAT, etc.)
using namespace testing;

#include <string>       // std::string
#include <exception>    // std::exception
#include <stdexcept>    // std::out_of_range, std::invalid_argument
#include <typeinfo>     // std::bad_cast

// ===========================================================================
// ===========================================================================
// ===========================================================================

// cs225::Component, cs225::ICloneable, cs225::IPersistable, cs225::IComparable  
#include "component.hh" 

/*********************************************************************
 *                          Component tests                          *
 *********************************************************************/

namespace Tests { namespace Component
{

// [ Test #1 ] -------------------------------------------------------
TEST( "Component is not directly instantiable, but usable only as a pointer/reference",
      "Component is an abstract base class, that has at least one unimplemented pure virtual function" )
{
    // this should not compile
    //cs225::Component i_shall_not_compile;
    
    // this should however be OK
    cs225::Component* this_is_ok;
    
    SUCCEED();
}

// minimum instantiable component
struct ConcreteComponent : public cs225::Component
{
    ConcreteComponent(int i = 0) : data(i) {}

    void load( std::istream& is )
    {
        is >> data;
    }
    void save( std::ostream& os ) const
    {
        os << data;
    }
    int compare_to( const IComparable& ) const
    {   // not an actual working implementation
        return 0;
    }
    
    ConcreteComponent* clone() const { return new ConcreteComponent(*this); }

    const char* type() const { return "concrete component"; }
    
    int data;
};

// [ Test #2 ] -------------------------------------------------------
TEST( "Requirement description for making a component concrete and instantiable",
      "Check the interface provided by the ConcreteComponent class and make sure that they are related to the appropriate methods provided by base classes" )
{
    ConcreteComponent concrete_component;
    
    // nothing to test, just make sure that this can compile and run
    SUCCEED();
}

// [ Test #3 ] -------------------------------------------------------
TEST( "Components can be cloned by overriding the corresponding function",
      "The cloning interface provides an operation that can act as a virtual constructor"  )
{
    ConcreteComponent concrete_component(9);
    const cs225::ICloneable * cloneable_interface = &concrete_component;

    cs225::ICloneable * cloned = cloneable_interface->clone();
    // cast so that we can access the 'type' method
    const cs225::Component * cloned_component = dynamic_cast<const cs225::Component *>(cloned);
    ASSERT_THAT( cloned_component != NULL );
    const std::string clone_type = cloned_component->type();

    ASSERT_THAT( clone_type == "concrete component" );

    delete cloned_component;
}

// [ Test #4 ] -------------------------------------------------------
TEST( "Components can be serialized to a stream by overriding the corresponding function",
      "The serialization interface provides a save (to stream) and load (from stream) operations" )
{
    ConcreteComponent concrete_component;
    cs225::IPersistable * persistable_interface = &concrete_component;

    // stringstream is both an istream and an ostream
    std::stringstream dummy_stream;
    // should load/save the component using the istream/ostream
    // this is only for interface requirement purposes
    persistable_interface->load(dummy_stream);
    persistable_interface->save(dummy_stream);

    SUCCEED();
}

// this is a class that happens to be comparable, 
// but has nothing to do with components
class ComparableString : public cs225::IComparable
{
public:
    explicit ComparableString(const std::string & str)
        : data(str) {}
    int compare_to(const IComparable &) const
    {   // placeholder implementation
        return 0;
    }
private:
    std::string data;
};

// [ Test #5 ] -------------------------------------------------------
TEST( "Components can be compared to each other by overriding the corresponding function",
      "The comparison interface provides an entry point to compare all objects that belong to classes that implement it" )
{
    ConcreteComponent concrete_component_a(3);
    ConcreteComponent concrete_component_b(5);

    const cs225::IComparable& comparable = concrete_component_a;

    try
    {
        int comparison_result = comparable.compare_to(concrete_component_b);
        // not performing any actual comparison, just testing the interface
        ASSERT_THAT( comparison_result == 0 );

    } catch(const std::bad_cast&) {
        std::cerr << "operands are not compatible for comparison\n";
        FAIL();
    }
}

} } // namespace Test::Component



// ===========================================================================
// ===========================================================================
// ===========================================================================


// cs225::Entity
#include "entity.hh" 

/*******************************************************************
 *                          Entity tests                           *
 *******************************************************************/

namespace Tests { namespace Entity
{

using Tests::Component::ConcreteComponent;

// concrete components for testing purposes
struct ConcreteComponentA : public ConcreteComponent
{
    const char * type() const { return "A"; }
    ConcreteComponent * clone() const { return new ConcreteComponentA(*this); }
    int a;
};
struct ConcreteComponentB : public ConcreteComponent
{
    ConcreteComponentB(int val) : b(val) {}
    const char * type() const { return "B"; }
    ConcreteComponent * clone() const { return new ConcreteComponentB(*this); }
    int b;
};

// [ Test #6 ] -------------------------------------------------------
TEST( "Entities internally store a collection/container of components",
      "Somehow each entity instance should provide storage for the components that define it" )
{
    cs225::Entity entity;
    
    // initially there should be 0 components
    ASSERT_THAT( entity.component_count() == 0u );
}


// [ Test #7 ] -------------------------------------------------------
TEST( "Multiple, potentially different concrete components can be attached to entities",
      "The attach function takes a pointer to a polymorphic instance, that internally is handled homogeneously" )
{
    cs225::Entity entity;

    entity.attach( new ConcreteComponentA() );
    entity.attach( new ConcreteComponentB(2) );
    entity.attach( new ConcreteComponentB(4) );

    ASSERT_THAT( entity.component_count() == 3u );
}

// [ Test #8 ] -------------------------------------------------------
TEST( "Entity class provides an operation to inspect specific components",
      "The subscript operator is overloaded on the Entity class to provide access to components by index" )
{
    cs225::Entity entity;

    entity.attach( new ConcreteComponentA() );
    entity.attach( new ConcreteComponentB(2) );
    entity.attach( new ConcreteComponentB(4) );

    // check the component @ index 1
    const cs225::Component& b2 = entity[1];

    ASSERT_THAT( std::string("B") == b2.type() );

    // downcast to access the derived data
    const ConcreteComponentB& ccb = dynamic_cast<const ConcreteComponentB&>(b2);
    ASSERT_THAT( ccb.b == 2 );
}

// [ Test #9 ] -------------------------------------------------------
TEST( "Accessing an out-of-bounds index results in an exception thrown",
      "Implement the InvalidIndex exception class; check the validity of the index parameter in the component accessor method" )
{
    cs225::Entity entity;

    entity.attach( new ConcreteComponentA() );

    try
    {
        const cs225::Component & no_such_component = entity[8];
        
        FAIL();

    } catch(const cs225::InvalidIndex & ex) {
        ASSERT_THAT( ex.value() == 8u );
    }
}

// [ Test #10 ] -------------------------------------------------------
TEST( "Performing a copy of the entity should have all its parts duplicated into the target copy",
      "Make sure that the copy constructor performs a deep copy and that the polymorphic components are duplicated with the appropriate mechanism" )
{
    cs225::Entity the_entity;
    the_entity.attach( new ConcreteComponentA() );
    the_entity.attach( new ConcreteComponentB(2) );
    the_entity.attach( new ConcreteComponentB(4) );

    cs225::Entity entity_copy(the_entity);

    ASSERT_THAT( entity_copy.component_count() == 3u );
    ASSERT_THAT( std::string("A") == entity_copy[0].type() );
    ASSERT_THAT( std::string("B") == entity_copy[1].type() );
    ASSERT_THAT( std::string("B") == entity_copy[2].type() );
}

// [ Test #11 ] -------------------------------------------------------
TEST( "Each entity has the responsibility of managing the components they get attached",
      "Cleaning up resources should be the owners responsibility. Check your memory leaks." )
{
    cs225::Component * standalone_cmps[] =
    {
        new ConcreteComponentA, new ConcreteComponentB(1), new ConcreteComponentB(3),
        new ConcreteComponentB(11), new ConcreteComponentA, new ConcreteComponentB(0),
    };
	
	// WORK IN DESTRUCTOR, LOOP UNTIL ALL COMPONENTS ARE DELETED
	
    {
        cs225::Entity the_entity;
        for( unsigned int i = 0; i < 6; ++i )
            the_entity.attach(standalone_cmps[i]);
    }

    SUCCEED();
}

// [ Test #12 ] -------------------------------------------------------
TEST( "Components can be also attached by reference",
      "Provide an overloaded function that takes the appropriate type; everything outside of this function implementation should remain the same" )
{
    cs225::Entity the_entity;

    {
        ConcreteComponentA cmp_a;
        the_entity.attach( cmp_a );
        the_entity.attach( ConcreteComponentB(2) );
    }
	
	// USE CLONING, POSSIBLY BY USING ATTACH WITH A REFERENCE AND CALLING OTHER ATTACH WITH CLONE

    ASSERT_THAT( the_entity.component_count() == 2u );
    ASSERT_THAT( std::string("A") == the_entity[0].type() );
    ASSERT_THAT( std::string("B") == the_entity[1].type() );
}

// [ Test #13 ] -------------------------------------------------------
TEST( "Components can access their respective owner entities",
      "All components internally store a pointer to their corresponding owners, for convenience" )
{
    cs225::Entity the_entity;
    the_entity.attach( new ConcreteComponentA() );
    the_entity.attach( new ConcreteComponentB(2) );

    ConcreteComponentA orphan_cmp;
    cs225::Component & owned_cmp = the_entity[0];
	
	// CREATE GETTOR OWNER, AND SETTER -> INITIALIZE PRIVATE POINTER TO NULL, AND IN ATTACH FUNC MAKE A POINTER POINT AT BASE

    ASSERT_THAT( orphan_cmp.owner() == NULL );
    ASSERT_THAT( owned_cmp.owner() == &the_entity );
}

// [ Test #14 ] -------------------------------------------------------
TEST( "Helper method to find and retrieve a concrete component type in an entity",
      "Iteratively test whether each component matches the requested type (as indicated by the template parameter); return the first occurence that matches, or NULL if not found" )
{
    cs225::Entity the_entity;
    the_entity.attach( ConcreteComponentA() );
    the_entity.attach( ConcreteComponentB(2) );
	
	// LOOP UNTIL DYNAMIC_CAST OF T (VECTOR[]) -> IS OF THE SAME TYPE

    ConcreteComponentB * found = the_entity.get_component_type<ConcreteComponentB>();

    ASSERT_THAT(found != NULL);
    ASSERT_THAT(found->b == 2);
}

} } // namespace Tests::Entity


// ===========================================================================
// ===========================================================================
// ===========================================================================

// cs225::chess::BoardPosition, cs225::chess::InvalidBoardPosition, cs225::chess::Color
#include "chess_board.hh" 

/********************************************************************
 *                        Chess board tests                         *
 ********************************************************************/

// Chess board representation:
//							# - Black
//    8 |_|#|_|#|_|#|_|#|
//    7 |#|_|#|_|#|_|#|_|
//    6 |_|#|_|#|_|#|_|#|
//    5 |#|_|#|_|#|_|#|_|
//    4 |_|#|_|#|_|#|_|#|
//    3 |#|_|#|_|#|_|#|_|
//    2 |_|#|_|#|_|#|_|#|
//    1 |#|_|#|_|#|_|#|_|
//       a b c d e f g h

namespace Tests { namespace Chess { namespace Board
{

// [ Test #14 ] -------------------------------------------------------
TEST( "The board coordinate representation uses an int-character pair to represent individual board squares",
      "Consider whether you should store the data as it is provided or if you should normalize it into a more usable range (like 0 to N) inside the corresponding methods" )
{
    const cs225::chess::BoardPosition top_left(8, 'a');
    
    cs225::chess::BoardPosition position_mutable(2, 'g');
    position_mutable.set(3, 'd');

    ASSERT_THAT( top_left.row()            == 8   );
    ASSERT_THAT( top_left.column()         == 'a' );
    ASSERT_THAT( position_mutable.row()    == 3   );
    ASSERT_THAT( position_mutable.column() == 'd' );
}

// [ Test #15 ] -------------------------------------------------------
TEST( "BoardPosition representations cannot adopt values outside of the chess board range values",
      "Check the boundaries in the offending functions and throw an instance of the custom exception class if necessary" )
{
    try
    {
        // no such position
        const cs225::chess::BoardPosition invalid(9, 'y');
        
        FAIL();
    } catch(const cs225::chess::InvalidBoardPosition& pos) {
        ASSERT_THAT( (pos.row() == 9) && (pos.column() == 'y') );
    }

    try
    {
        cs225::chess::BoardPosition illegal_move(2, 'g');
        illegal_move.set(32, 'b'); // attempting a move to an illegal position
        
        FAIL();
    } catch(const cs225::chess::InvalidBoardPosition& pos){
        ASSERT_THAT( (pos.row() == 32) && (pos.column() == 'b') );
    }
}


// [ Test #16 ] -------------------------------------------------------
TEST( "The color of the underlying square can be determined by a function",
      "Implement a method that determines whether the square is white or black based on the standard coordinates described on the ascii art diagram" )
{
    cs225::chess::BoardPosition board_square(8, 'a');

    cs225::chess::Color top_left_color = board_square.color();
    board_square.set(1, 'a');
    cs225::chess::Color bottom_left_color = board_square.color();

    ASSERT_THAT( top_left_color    == cs225::chess::Color::white);
    ASSERT_THAT( bottom_left_color == cs225::chess::Color::black );
}

} } } // namespace Tests::Chess::Board



// ===========================================================================
// ===========================================================================
// ===========================================================================


// cs225::chess::Piece, cs225::chess::PieceMovement, cs225::chess::*Movement 
#include "chess_piece.hh" 

/********************************************************************
 *                       Chess pieces tests                         *
 ********************************************************************/


namespace Tests { namespace Chess { namespace Pieces
{

// [ Test #17 ] -------------------------------------------------------
TEST( "Chess pieces are represented as a kind of entity",
      "Make sure that Piece and Entity are (polymorphically) compatible" )
{
    cs225::chess::Piece a_piece;
    try
    {
        // nothing to test, just make sure that this can be done
        cs225::Entity & as_entity = dynamic_cast<cs225::Entity &>(a_piece);
        
    } catch(std::bad_cast &){
        FAIL();
    }
}

// [ Test #18 ] -------------------------------------------------------
TEST( "A chess piece position state is represented by a Position component",
      "Create a concrete component that encapsulates what is required to represent the position of a chess piece on the board (should be using the BoardPosition class for consistency)" )
{
	// Position	: public Component -> should represent the position of a chess piece using chess board
	
	
	
    cs225::chess::Piece a_piece;
	
	// TODO function of attaching to entity of Piece
    a_piece.attach( cs225::chess::Position(2, 'd') );
	
	// TODO check type function
    ASSERT_THAT( std::string("position") == a_piece[0].type() );

	
    try
    {
        // access position details
        cs225::chess::Position & position_cmp = dynamic_cast<cs225::chess::Position & >(a_piece[0]);
        
        ASSERT_THAT( position_cmp == cs225::chess::BoardPosition(2, 'd') );
    
    } catch(std::bad_cast &){
        std::cerr << "the attached object should be convertible back to a Position instance\n";
        FAIL();
    }
}

// [ Test #19 ] -------------------------------------------------------
TEST( "When drawing the piece, visual representation is provided by a dedicated component",
      "Create a component that encapsulates the visual representational details for chess pieces" )
{
    cs225::chess::Piece a_pawn;
    a_pawn.attach( cs225::chess::PieceVisuals('p', cs225::chess::Color::white) );

    try
    {
        // access position details
        cs225::chess::PieceVisuals & pawn_representation = dynamic_cast<cs225::chess::PieceVisuals & >(a_pawn[0]);
        
        ASSERT_THAT( pawn_representation.shape() == 'p' );
        ASSERT_THAT( pawn_representation.color() == cs225::chess::Color::white );

    } catch(std::bad_cast &){
        std::cerr << "the attached object should be convertible back to a PieceVisuals instance\n";
        FAIL();
    }
}

// [ Test #20 ] -------------------------------------------------------
TEST( "Piece movement capabilities are provided by dedicated concrete components",
      "Notice the difference between the interface PieceMovement and the concrete implementation on PawnMovement. Try to implement this using the Non-virtual interface idiom." )
{
    cs225::chess::Piece a_pawn;
    a_pawn.attach( cs225::chess::Position(7, 'd') );
    a_pawn.attach( cs225::chess::PawnMovement() );

    cs225::chess::PieceMovement * movement = a_pawn.get_component_type<cs225::chess::PieceMovement>();
    ASSERT_THAT(movement != NULL);

    // considering its current position, we'll attempt some moves
    cs225::chess::BoardPosition valid_target(6, 'd');   // a pawn can move like this
    cs225::chess::BoardPosition invalid_target(3, 'a'); // but not like this

    // the PawnMovement class should provide the implementation for the pawn movement rules
    // implicitly takes the source position to be the one described by the sibling position component
    ASSERT_THAT( movement->is_valid_move(valid_target) == true );
    ASSERT_THAT( movement->is_valid_move(invalid_target) == false );
}

/**********************************
 * Movement rules for each piece  *
 **********************************/
// ||                                                    _:_       ||
// ||                                                   '-.-'      ||
// ||                                          ()      __.'.__     ||
// ||                                       .-:--:-.  |_______|    ||
// ||                                ()      \____/    \=====/     ||
// ||                                /\      {====}     )___(      ||
// ||                     (\=,      //\\      )__(     /_____\     ||
// ||     __    |'-'-'|  //  .\    (    )    /____\     |   |      ||
// ||    /  \   |_____| (( \_  \    )__(      |  |      |   |      ||
// ||    \__/    |===|   ))  `\_)  /____\     |  |      |   |      ||
// ||   /____\   |   |  (/     \    |  |      |  |      |   |      ||
// ||    |  |    |   |   | _.-'|    |  |      |  |      |   |      ||
// ||    |__|    )___(    )___(    /____\    /____\    /_____\     ||
// ||   (====)  (=====)  (=====)  (======)  (======)  (=======)    ||
// ||   }===={  }====={  }====={  }======{  }======{  }======={    ||
// ||  (______)(_______)(_______)(________)(________)(_________)   ||


// [ Test #21 ] -------------------------------------------------------
TEST( "Pawn movement rules implementation",
      "Implement the necessary logic in the PawnMovement class" )
{
    using cs225::chess::BoardPosition;

    cs225::chess::Piece pawn;
    pawn.attach( cs225::chess::Position(7, 'd') ); // current position
    pawn.attach( cs225::chess::PawnMovement() );   // movement rules

    // not meant to be exhaustive
    // no need to check for advanced movements, like 'en passant'
    cs225::chess::BoardPosition one_forward(6, 'd'); // valid
    cs225::chess::BoardPosition sideways(7, 'c');    // invalid
    cs225::chess::BoardPosition diagonal(6, 'e');    // invalid, if no enemy piece present

    // since it will be such a common operation, we'll provide the 
    // convenience method 'is_valid_move' in the Piece class
    ASSERT_THAT( pawn.is_valid_move(one_forward) );
    ASSERT_THAT( pawn.is_valid_move(sideways) == false );
    ASSERT_THAT( pawn.is_valid_move(diagonal) == false );
}


// [ Test #22 ] -------------------------------------------------------
TEST( "Bishop movement rules implementation",
      "Implement the necessary logic in the BishopMovement class" )
{
    using cs225::chess::BoardPosition;

    cs225::chess::Piece bishop;
    cs225::chess::BoardPosition current_position(1, 'c');
    bishop.attach( cs225::chess::Position(current_position) ); // current position
    bishop.attach( cs225::chess::BishopMovement() );   // movement rules

    cs225::chess::Color bishop_color = current_position.color();
	

    // some valid moves
    cs225::chess::BoardPosition diagonals[] = 
    {
        BoardPosition(2, 'd'), BoardPosition(4, 'f'), BoardPosition(6, 'h')
    };
    // some invalid moves
    cs225::chess::BoardPosition invalid_moves[] = 
    {
        BoardPosition(1, 'a'), BoardPosition(5, 'd'), BoardPosition(2, 'g')
    };
 
	
    for( unsigned int i = 0; i < 3; ++i )
    {
        ASSERT_THAT( (bishop.is_valid_move(diagonals[i])) 
                  && (diagonals[i].color() == bishop_color) );
        ASSERT_THAT( bishop.is_valid_move(invalid_moves[i]) == false );
    }
}

// [ Test #23 ] -------------------------------------------------------
TEST( "Rook movement rules implementation",
      "Implement the necessary logic in the RookMovement class" )
{
    using cs225::chess::BoardPosition;

    cs225::chess::Piece rook;
    cs225::chess::BoardPosition current_position(4, 'c');
    rook.attach( cs225::chess::Position(current_position) ); // current position
    rook.attach( cs225::chess::RookMovement() );   // movement rules

    // some valid moves
    cs225::chess::BoardPosition valid_moves[] = 
    {
        BoardPosition(8, 'c'), BoardPosition(4, 'h'), BoardPosition(1, 'c')
    };
    // some invalid moves
    cs225::chess::BoardPosition invalid_moves[] = 
    {
        BoardPosition(3, 'b'), BoardPosition(2, 'g'), BoardPosition(7, 'g')
    };

    for( unsigned int i = 0; i < 3; ++i )
    {
        ASSERT_THAT( rook.is_valid_move(valid_moves[i]) );
        ASSERT_THAT( rook.is_valid_move(invalid_moves[i]) == false );
    }
}
// [ Test #24 ] -------------------------------------------------------
TEST( "Knight movement rules implementation",
      "Implement the necessary logic in the KnightMovement class" )
{
    using cs225::chess::BoardPosition;

    cs225::chess::Piece knight;
    cs225::chess::BoardPosition current_position(4, 'd');
    knight.attach( cs225::chess::Position(current_position) ); // current position
    knight.attach( cs225::chess::KnightMovement() );   // movement rules

    // some valid moves
    cs225::chess::BoardPosition valid_moves[] = 
    {
        BoardPosition(2, 'c'), BoardPosition(3, 'b'), BoardPosition(6, 'e')
    };
    // some invalid moves
    cs225::chess::BoardPosition invalid_moves[] = 
    {
        BoardPosition(6, 'b'), BoardPosition(2, 'd'), BoardPosition(7, 'e')
    };

    for( unsigned int i = 0; i < 3; ++i )
    {
        ASSERT_THAT( knight.is_valid_move(valid_moves[i]) );
        ASSERT_THAT( knight.is_valid_move(invalid_moves[i]) == false );
    }
}

// [ Test #25 ] -------------------------------------------------------
TEST( "King movement rules implementation",
      "Implement the necessary logic in the KingMovement class" )
{
    using cs225::chess::BoardPosition;

    cs225::chess::Piece king;
    cs225::chess::BoardPosition current_position(4, 'd');
    king.attach( cs225::chess::Position(current_position) ); // current position
    king.attach( cs225::chess::KingMovement() );   // movement rules

    // some valid moves
    cs225::chess::BoardPosition valid_moves[] = 
    {
        BoardPosition(5, 'd'), BoardPosition(4, 'e'), BoardPosition(3, 'c')
    };
    // some invalid moves
    cs225::chess::BoardPosition invalid_moves[] = 
    {
        BoardPosition(4, 'b'), BoardPosition(2, 'a'), BoardPosition(7, 'e')
    };

    for( unsigned int i = 0; i < 3; ++i )
    {
        ASSERT_THAT( king.is_valid_move(valid_moves[i]) );
        ASSERT_THAT( king.is_valid_move(invalid_moves[i]) == false );
    }
}

// [ Test #26 ] -------------------------------------------------------
TEST( "Queen movement rules implementation",
        "Implement the necessary logic in the QueenMovement class" )
{
    using cs225::chess::BoardPosition;

    cs225::chess::Piece queen;
    cs225::chess::BoardPosition current_position(4, 'd');
    queen.attach( cs225::chess::Position(current_position) ); // current position
    queen.attach( cs225::chess::QueenMovement() );   // movement rules

    // some valid moves
    cs225::chess::BoardPosition valid_moves[] = 
    {
        BoardPosition(7, 'a'), BoardPosition(8, 'h'), BoardPosition(4, 'g')
    };
    // some invalid moves
    cs225::chess::BoardPosition invalid_moves[] = 
    {
        BoardPosition(6, 'e'), BoardPosition(5, 'a'), BoardPosition(1, 'h')
    };

    for( unsigned int i = 0; i < 3; ++i )
    {
        ASSERT_THAT( queen.is_valid_move(valid_moves[i]) );
        ASSERT_THAT( queen.is_valid_move(invalid_moves[i]) == false );
    }
}


} } } // namespace Tests::Chess::Pieces

// ===========================================================================
// ===========================================================================
// ===========================================================================






