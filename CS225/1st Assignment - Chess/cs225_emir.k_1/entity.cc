/**********************************************************************************/
/*!
\file   	entity.cc
\language 	C++
\author 	Emir Khanseitov
\par    	email: emir.k@digipen.edu
\par    	digiPen login: emir.k
\par    	Course: CS225
\par    	Assignment #1 - Chess
\date   	28/09/2018
\updated 	12/10/2018
\brief  
    Contains functions definitions of Entity class as well as an exception class
        

\par Hours spent on this assignment: 
    30-40 hours, very desorganized hours


\par Specific portions that gave you the most trouble: 
    - Understanding concepts behind entity-component systems 
	- Managing the time around the assignment
	- Going straight on without stopping programming while coding the assignments
  
*/
/**********************************************************************************/

#include "entity.hh"
#include <stdio.h>

namespace cs225	{
	
	// Constructor for initialization of m_components_counter
	Entity::Entity() : m_components_counter(0) {}
	
	// Copy constructor for deep copies
	Entity::Entity( const Entity& other_entity)
	{
		// initialize the m_components_counter of THIS entity
		m_components_counter = 0;
	
		// Loop to copy all the components and NOT pointers
		for(unsigned int i = 0; i < other_entity.m_components_counter; ++i)
		{
			// Clone the component from other_entity
			attached_components.push_back(other_entity[i].clone());
			
			// Increment the m_components_counter
			++m_components_counter;
		}
	}

	// Destructor re-definition for proper de-allocation
	Entity::~Entity()
	{
		// Loop until all components are disattached and destroyed
		while(attached_components.empty() == false)
		{
			// pop the last element
			attached_components.pop_back();
			
			// decrease number of components in entity
			--m_components_counter;
		}
	}

	// function to see how many components there are
	unsigned int Entity::component_count() const { return m_components_counter; }
	
	// function that attaches Components to the entity
	void Entity::attach( Component* added_comp )
	{
		// Set the owner for the component as this entity
		added_comp->SetOwner(this);
	
		// Attach the component itself
		attached_components.push_back( added_comp );
		
		// increment the component counter
		++m_components_counter;
	}
	
	// overload of the function that attaches comps by reference
	// Blai and Massimo helped me with this function. 
	// remember -> temporal objects must be called by const &... and then cloned
	void Entity::attach( const Component& ref_comp ) { attach(ref_comp.clone());}
	
	// Operator overload for [] to access a particular component
	Component& Entity::operator[](unsigned int index) const
	{
		// Sanity check for going out of bounds
		if(index > m_components_counter)
			throw(InvalidIndex(index));
	
		// return the particular member of the vector
		return *attached_components[index];
	}
	
	
	// Non-default Constructor for initialization of wrong_value 
	InvalidIndex::InvalidIndex(unsigned int index) : wrong_value(index) {}
	
	// Getter function for value in InvalidIndex 
	unsigned int InvalidIndex::value() const { return wrong_value; }
	
}