
#pragma once

namespace colors
{
    const int white = 7;
    const int green = 2;
    const int red = 4;
    const int yellow = 6;
    const int blue = 1;
}

#if (  ( defined(__WIN32) || defined(_WIN32) || defined(_WIN64) ) \
    && ( defined(USE_COLORED_OUTPUT) ) )

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <iostream>


template <typename T>
void print( const T& msg, int color = colors::white )
{
    HANDLE console = ::GetStdHandle(STD_OUTPUT_HANDLE);
    ::SetConsoleTextAttribute(console, color);
    std::cout << msg;
    ::SetConsoleTextAttribute(console, colors::white);
}

#else // colored output does not have an effect if 
      // disabled or run on a non-windows platform

#include <iostream>

template <typename T>
void print( const T& msg, int )
{
    std::cout << msg;
}

#endif
