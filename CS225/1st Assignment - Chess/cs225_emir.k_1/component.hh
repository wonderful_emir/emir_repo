/**********************************************************************************/
/*!
\file   	component.hh
\language 	C++
\author 	Emir Khanseitov
\par    	email: emir.k@digipen.edu
\par    	digiPen login: emir.k
\par    	Course: CS225
\par    	Assignment #1 - Chess
\date   	28/09/2018
\updated 	12/10/2018
\brief  
    Contains several classes tied to Components as well as function declarations
	for said classes, including virtual ones
        

\par Hours spent on this assignment: 
    30-40 hours, very desorganized hours


\par Specific portions that gave you the most trouble: 
    - Understanding concepts behind entity-component systems 
	- Managing the time around the assignment
	- Going straight on without stopping programming while coding the assignments
  
*/
/**********************************************************************************/

#pragma once
#include <iostream>



// Tests go through cs225 namespace
namespace cs225 {

	// forward declaration of entity class
	class Entity;

	// Component inherits from all interfaces below
	class ICloneable
	{
	public:
	
/*!****************************************************************************
 \fn	 ~ICloneable()
 
 \brief	 Virtual destructor

 \param void

 \return void
******************************************************************************/
		virtual ~ICloneable() {};
		
/*!****************************************************************************
 \fn	 clone() const 
 
 \brief
Pure virtual function of clone() for cloning the same component

 \param void

 \return ICloneable*
******************************************************************************/
		virtual ICloneable* clone() const = 0;
	};
	
	class IPersistable
	{
	public:
/*!****************************************************************************
 \fn	 ~IPersistable()
 
 \brief	 Virtual destructor

 \param void

 \return void
******************************************************************************/
		virtual ~IPersistable() {};
		
/*!****************************************************************************
 \fn	 load( std::istream& is )
 
 \brief	 load function for serialization

 \param is
std::istream&

 \return void
******************************************************************************/
		virtual void load( std::istream& is ) = 0;
		
/*!****************************************************************************
 \fn	 save( std::ostream& os ) const 
 
 \brief	 save function to store in a text file

 \param os
 std::ostream&

 \return void
******************************************************************************/
		virtual void save( std::ostream& os ) const = 0;
	};
	
	class IComparable
	{
	public:
/*!****************************************************************************
 \fn	 ~IComparable()
 
 \brief	 Virtual destructor

 \param void

 \return void
******************************************************************************/
		virtual ~IComparable() {};
		
/*!****************************************************************************
 \fn	 compare_to( const IComparable& ) const 
 
 \brief	 function to compare number of components (I suppose)

 \param  ...
const IComparable& 

 \return int
******************************************************************************/
		virtual int compare_to( const IComparable& ) const = 0;
	};

	
	class Component : public ICloneable, public IPersistable, public IComparable
	{
	public:
/*!****************************************************************************
 \fn	 Component() 
 
 \brief	 Constructor for initialization of the pointer to parent

 \param  void

 \return void
******************************************************************************/
		Component();
	
/*!****************************************************************************
 \fn	 ~Component()
 
 \brief	 Virtual destructor

 \param void

 \return void
******************************************************************************/
		virtual ~Component() {};
		
/*!****************************************************************************
 \fn	 clone() const 
 
 \brief	 pure virtual function to clone itself

 \param  void

 \return Component*
******************************************************************************/
		virtual Component* clone() const = 0;
		
/*!****************************************************************************
 \fn	 type() const 
 
 \brief	 pure virtual function to get the type of the Component

 \param  void

 \return const char*
******************************************************************************/
		virtual const char* type() const = 0;
		
/*!****************************************************************************
 \fn	 owner() const 
 
 \brief	 Getter function to get the pointer to the parent

 \param  void

 \return Entity*
******************************************************************************/
		Entity* owner() const;
		
/*!****************************************************************************
 \fn	 owner() const 
 
 \brief	 Setter function to set the pointer to a parent

 \param  base
Entity*

 \return void
******************************************************************************/
		void SetOwner(Entity* base);
		
	private:
		// pointer to the base
		Entity * ptr_to_base;
		
	}; // class Component
	
	
	
	
} // namespace cs225