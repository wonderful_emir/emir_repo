/**********************************************************************************/
/*!
\file   	chess_piece.cc
\language 	C++
\author 	Emir Khanseitov
\par    	email: emir.k@digipen.edu
\par    	digiPen login: emir.k
\par    	Course: CS225
\par    	Assignment #1 - Chess
\date   	28/09/2018
\updated 	12/10/2018
\brief  
    Supposedly runs and not-poorly implemented version of a chess game, but sadly due
	to lack of time is downgraded to a skeleton draft of work. next time assignments 
	should be started upon very much earlier
        
		
\par Hours spent on this assignment: 
    30-40 hours, very desorganized hours


\par Specific portions that gave you the most trouble: 
    - Understanding concepts behind entity-component systems 
	- Managing the time around the assignment
	- Going straight on without stopping programming while coding the assignments
  
*/
/**********************************************************************************/

#include <iostream>
#include "chess_piece.hh"


// MISSING
// The pieces being displayed, moving the piece, ASSERTS for moving incorrectly, eating the piece 
// win condition? when you eat the king, Turn Based from black to white

// EXTRA Brainstorm on making this a cool game implementation
// Implementation of normal game with chess, but you have only 5 seconds for a turn
// Choose options -> normal game, 5 second rule, 25 second rule...and so on
// Even better if -> you have less possibilities of where to go, as time goes on


namespace cs225{ namespace chess{ 
/*!****************************************************************************
 \fn	 Create_board()
 
 \brief	 Function to create the board -> white spaces/black spaces/black

 \param  void

 \return BoardPosition *
******************************************************************************/
BoardPosition * Create_board()
{
	// Counters to create the cell with appropriate color
	int Rows_counter = 1;
	char Columns_counter = 'a';
	
	// Number of cells is number of maximum rows and columns
	int num_of_Cells = 8*8;
	
	// Array of BoardPositions
	BoardPosition * My_board = new BoardPosition[num_of_Cells];
	
	// Loop until number of all 8*8 = 64 cells were set
	for(int i = 0; i < num_of_Cells; ++i, ++Columns_counter)
	{
		//If Rows_counter reached it's maximum we reset it, and increment counter of columns
		if(Columns_counter > 'h')
		{
			Columns_counter = 'a'; 
			++Rows_counter;
		}
		
		//Create Boardposition appropriately
		My_board[i] = BoardPosition(Rows_counter, Columns_counter);
	}
	
	return My_board;
}


/*!****************************************************************************
 \fn	 Print_board()
 
 \brief	 Function that prints the board (And supposed to do pieces)

 \param  board_given
BoardPosition *
 
 \return void
******************************************************************************/
void Print_board(BoardPosition * board_given/*, Piece * pieces_given*/)
{
	// Board size and one row
	int board_size = 8*8;
	int row_size = 8;
	
	// Counters show the cell with appropriate color (Row is 7, because arrays start at 0)
	int Rows_counter = 7;
	char Columns_counter = 0;
	
// Chess board representation:
//							# - Black
//    8 |_|#|_|#|_|#|_|#|
//    7 |#|_|#|_|#|_|#|_|
//    6 |_|#|_|#|_|#|_|#|
//    5 |#|_|#|_|#|_|#|_|
//    4 |_|#|_|#|_|#|_|#|
//    3 |#|_|#|_|#|_|#|_|
//    2 |_|#|_|#|_|#|_|#|
//    1 |#|_|#|_|#|_|#|_|
//       a b c d e f g h

// Chess board with Pieces:
//							# - Black
//    8 |r|k|b|q|$|b|k|r|
//    7 |p|p|p|p|p|p|p|p|
//    6 |_|#|_|#|_|#|_|#|
//    5 |#|_|#|_|#|_|#|_|
//    4 |_|#|_|#|_|#|_|#|
//    3 |#|_|#|_|#|_|#|_|
//    2 |P|P|P|P|P|P|P|P|
//    1 |R|K|B|Q|@|B|K|R|
//       a b c d e f g h
	
	// Print out first number
	std::cout<<"8 ";
	
	// Print the board looping
	for(int i = 0/*, j = 0, k = 0*/; i < board_size; ++i, ++Columns_counter/*, ++j*/)
	{
		// Reset counter of columns if max, and decrease counter of rows
		if(Columns_counter > 7)
		{
			// Also print out the number of row for readability
			std::cout<<"|"<<std::endl<<Rows_counter << " ";
			
			Columns_counter = 0; 
			--Rows_counter;
		}
		
		// Foundation onto making The pieces appear
		//if((j < 32 && i < 16))
		//	std::cout<<"|"<<
		
		// |#| -> Black cell, |_| -> white cell
		if(board_given[Columns_counter + Rows_counter * row_size].color() == Color::white)
			std::cout<<"|_";
		else //if (board_given[Columns_counter + Rows_counter * row_size].color() == Color::black)
			std::cout<<"|#";
	}
	
	// Print out the last closing cell
	std::cout<<"|"<<std::endl;
	
	// TODO: proper C++ formatting
	std::cout<<"   a b c d e f g h "<<std::endl;
}


/*!****************************************************************************
 \fn	 Create_Pieces()
 
 \brief	 Function that creates the pieces, (but doesn't print them :( )

 \param  void
 
 \return Piece *
******************************************************************************/
Piece * Create_Pieces()
{	
	// Number of each piece and number of pieces overall
	int num_pieces = 16;

	// Vertical difference of placing blacks or whites
	int vert_diff_placem_pawns = 0;
	int vert_diff_placem_officers = 0;
	
	// Horizontal differences for officers
	int horiz_diff_placem_rooks = 0;
	int horiz_diff_placem_knights = 0;
	int horiz_diff_placem_bishops = 0;
	
	// Different symbol for different king
	char king_symbol = '$';
	
	// Create array to number of pieces, both WHITE and BLACK
	Piece * My_pieces = new Piece [num_pieces * 2];
	
	
	// Double loop for both components, start by pushing black Pieces
	for(int j = 0; j < 2; ++j)
	{
		// Fulffill array with all of the Pieces (with appropriate components) of one color Piece 
		for(int i = 0; i < num_pieces; ++i)
		{
			// Create a Piece
			Piece * thee_piece = new Piece;
			
			// Firstly pawns
			if(i < 8)
			{
				// Attach all necessary components to thee_piece
				thee_piece->attach(Position(7 + vert_diff_placem_pawns, 'a' + i));
				thee_piece->attach(PieceVisuals('p', j));
				thee_piece->attach(PawnMovement());
				
				My_pieces[i + j * num_pieces] = *thee_piece;
			}
			
			// then officers - now Rook
			else if(i < 10)
			{
				// Attach all necessary components to thee_piece
				thee_piece->attach(Position(8 + vert_diff_placem_officers, 'a' + horiz_diff_placem_rooks));
				thee_piece->attach(PieceVisuals('r', j));
				thee_piece->attach(RookMovement());
				
				// Push it to dynamically allocated array of pieces
				My_pieces[i + j * num_pieces] = *thee_piece;
				
				// Remember to put different Rook on the other side of the board
				horiz_diff_placem_rooks = 7;
			}
			
			// Knight
			else if(i < 12)
			{
				// Attach all necessary components to thee_piece
				thee_piece->attach(Position(8 + vert_diff_placem_officers, 'b' + horiz_diff_placem_knights));
				thee_piece->attach(PieceVisuals('k', j));
				thee_piece->attach(KnightMovement());
				
				// Push it to dynamically allocated array of pieces
				My_pieces[i + j * num_pieces] = *thee_piece;
				
				// Remember to put different Knight on the other side of the board
				horiz_diff_placem_knights = 5;
			}
			
			// Bishop
			else if(i < 14)
			{
				// Attach all necessary components to thee_piece
				thee_piece->attach(Position(8 + vert_diff_placem_officers, 'c' + horiz_diff_placem_bishops));
				thee_piece->attach(PieceVisuals('b', j));
				thee_piece->attach(BishopMovement());
				
				// Push it to dynamically allocated array of pieces
				My_pieces[i + j * num_pieces] = *thee_piece;
				
				// Remember to put different Bishop on the other side of the board
				horiz_diff_placem_bishops = 3;
			}
			
			// Queen
			else if(i < 15)
			{
				// Attach all necessary components to thee_piece
				thee_piece->attach(Position(8 + vert_diff_placem_officers, 'd'));
				thee_piece->attach(PieceVisuals('q', j));
				thee_piece->attach(QueenMovement());
				
				// Push it to dynamically allocated array of pieces
				My_pieces[i + j * num_pieces] = *thee_piece;
			}
			
			// King
			else
			{
				// Attach all necessary components to thee_piece
				thee_piece->attach(Position(8 + vert_diff_placem_officers, 'e'));
				thee_piece->attach(PieceVisuals(king_symbol, j));
				thee_piece->attach(KingMovement());
				
				// Push it to dynamically allocated array of pieces
				My_pieces[i + j * num_pieces] = *thee_piece;
			}
		}
		
		// Reset the Horizontal counters, but bring value to the vertical ones 
		vert_diff_placem_pawns = -5;
		vert_diff_placem_officers = -7;

		horiz_diff_placem_rooks = 0;
		horiz_diff_placem_knights = 0;
		horiz_diff_placem_bishops = 0;
		
		// Also change the symbol of king
		king_symbol = '@';	
	}
	
	return My_pieces;
}
}} // namespace cs225::chess




/*!****************************************************************************
 \fn	 main()
 
 \brief	 Main loop of the game that couldn't have been very good :(

 \param  void
 
 \return int
******************************************************************************/
int main()
{
	// Answer of people
	char answer;
	
	// Let's introduce the chess
	std::cout<<"You just launched a game of poorly-implemented chess" << std::endl;
	std::cout<<"do you want to play it? Type: y/n" << std::endl<<std::endl;
	
	// Allow some input on the player first
	std::cin >> answer;
	
	// And if he types anything but y, we quit... or at least ty to, 
	// feel free to close this tree - Hoenstly I didn't waste too much time on that... :(
	if(answer != 'y')
	{
		std::cout<<"\n B-but then... did you launch it for no reason? Type: y/n"<<std::endl<<std::endl;
		std::cin >> answer;
		
		if(answer == 'y')
		{
			std::cout<<"\n Does that mean you just want to quit...? Type: y/n"<<std::endl<<std::endl;
			std::cin >> answer;
			
			if(answer == 'y')
			{
				std::cout<<"\n I won't let you quit! " <<
				"Use Ctrl + C if you so desire! "<< 
				"I like my Chess and you should try it! Will you? Type y/n" << std::endl<<std::endl;
				std::cin >> answer;
				
				if(answer == 'y')
				{
					std::cout<<"\n Okay, then let's forget this whole thing happened, "<<
					"and just launch my exe again :3"<<std::endl<<std::endl;
					return 0;
				}
				else
				{
					std::cout<<"\n Fine, you can go now"<<std::endl<<std::endl;
					return 0;
				}
			}
			else
			{
				std::cout<<"\n You are hell of a confusing person, I guess bye-bye! :~3"<<std::endl<<std::endl;
				return 0;
			}
		}
		else
		{
			std::cout<<"\n Okay, I guess then you can relaunch it :)"<<std::endl<<std::endl;
			return 0;
		}
		
		return 0;
	}
		
	else
	{
		// Boolean check if the win condition was reached
		bool nobody_won = true;
		bool white_turn = true;
		
		
		// We start the ultimate battle of chess
		std::cout<<"\n Then let's proceed with strategic battle of minds!" << std::endl<<std::endl;
		
		// Call functions to create the board and the pieces
		cs225::chess::BoardPosition* my_board = cs225::chess::Create_board();
		cs225::chess::Piece * my_pieces = cs225::chess::Create_Pieces();
		
		// Loop where the whol game happens
		while(nobody_won)
		{
			// Print the board and the pieces (TODO the pieces displaying)
			cs225::chess::Print_board(my_board/*, my_pieces*/);
			
			// Check for whose turn it is
			if(white_turn)
				std::cout<<std::endl<< "  <> WHITE TURN <>  " <<std::endl<<std::endl;
			else if (white_turn == false)
				std::cout<<std::endl<< "  <> BLACK TURN <>  " <<std::endl<<std::endl;
			
			// instructions on choosing which piece to take
			std::cout << "Choose which piece to move (yours are on the bottom of the board)"<<std::endl;
			std::cout << "Example: 2d"<<std::endl;
			
			std::cout<< "that way you move this pawn with this many components attached to it = " << 
														my_pieces[20].component_count() << " at position 2d" << std::endl;
			
			// Answer of the player
			std::cin >> answer;
			
			// TODO: Instructions on where to move it
			// Sanity checks if such move is allowed through is valid move
			// Sanity checks if it went beyond the board
			// Sanity checks on taking opponents piece disregard as not Illegal move
			
			// Actual move the piece and it moves the letter correctly implementation
			// Eating other pieces implementation which makes them being removed
			// Sanity checks on win condition (king eaten) 
			//							with nobody_won at that point becoming false
			//
			
			// Attempt at turn-based look 
			if(white_turn)
				white_turn = false;
			else
				white_turn = true;
			
			nobody_won = false;
		}
		
		// Remember to de-allocate the board and pieces memory
		//for(int i = 0; i < )
		return 0;
	}
}

