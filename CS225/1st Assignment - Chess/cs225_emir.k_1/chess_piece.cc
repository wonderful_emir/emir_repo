/**********************************************************************************/
/*!
\file   	chess_piece.cc
\language 	C++
\author 	Emir Khanseitov
\par    	email: emir.k@digipen.edu
\par    	digiPen login: emir.k
\par    	Course: CS225
\par    	Assignment #1 - Chess
\date   	28/09/2018
\updated 	12/10/2018
\brief  
    Contains definitions for the functions in the chess_piece.hh
        
		
\par Hours spent on this assignment: 
    30-40 hours, very desorganized hours


\par Specific portions that gave you the most trouble: 
    - Understanding concepts behind entity-component systems 
	- Managing the time around the assignment
	- Going straight on without stopping programming while coding the assignments
  
*/
/**********************************************************************************/

#include "chess_piece.hh"

// For abs in Movement Functions
#include <cstdlib>

namespace cs225 {	namespace chess	{
	
	// Piece helper fucntion is_valid_move
	bool Piece::is_valid_move(BoardPosition possible_move)
	{
		// Get the Movement of a particular piece
		PieceMovement* which_piece = this->get_component_type<PieceMovement>();
		
		return which_piece->is_valid_move(possible_move);
	}
	
	
	
	
	// Position component virtual function definitions
	Position::Position(int Row, char Column) : m_pos_on_board(Row, Column) {}
	Position::Position(BoardPosition pos) : m_pos_on_board(pos) {}
	Position::~Position() {}
	const char* Position::type() const {	return "position";	}
	void Position::load(std::istream& is)	{ if(is == NULL) return; }
	void Position::save(std::ostream& os) const { if(os == NULL) return; }
	Component* Position::clone() const { return new 
										Position(this->Get_row(), this->Get_column()); }
	int Position::compare_to(const IComparable&) const { return 0; }
	
	// Operator overload for == check if same as board position or not
	bool Position::operator== (BoardPosition the_board) const
	{
		// Check for row, then check for column
		if(m_pos_on_board.row() == the_board.row())
			if(m_pos_on_board.column() == the_board.column())
				return true;
				
		return false;
	}
	
	// Getters
	int Position::Get_row() const	{	return m_pos_on_board.row();	}
	char Position::Get_column() const	{	return m_pos_on_board.column();	}
	
	
	
	
	
	
	
	
	// Visuals of piece Component
	PieceVisuals::PieceVisuals(char The_piece, int which_color)	:	
										m_which_piece(The_piece), m_piece_color(which_color){}
	PieceVisuals::~PieceVisuals(){}
	const char* PieceVisuals::type() const {	return "PieceVisuals";	}
	void PieceVisuals::load(std::istream& is)	{ if(is == NULL) return; }
	void PieceVisuals::save(std::ostream& os) const { if(os == NULL) return; }
	Component* PieceVisuals::clone() const { return new PieceVisuals(this->shape(), this->color()); }
	int PieceVisuals::compare_to(const IComparable&) const { return 0; }
	
	// Getter functions
	char PieceVisuals::shape()  const {	return m_which_piece;	}
	int PieceVisuals::color() const	{	return m_piece_color;	}
		
		
		
		
		
		
		
	// PawnMovement 
	PawnMovement::~PawnMovement(){}
	const char* PawnMovement::type() const {	return "PawnMovement";	}
	void PawnMovement::load(std::istream& is)	{ if(is == NULL) return; }
	void PawnMovement::save(std::ostream& os) const { if(os == NULL) return; }
	Component* PawnMovement::clone() const { return new PawnMovement(); }
	int PawnMovement::compare_to(const IComparable&) const { return 0; }
	
	// PawnMovement Component function to check the validity of the move
	bool PawnMovement::is_valid_move(BoardPosition possible_move) const
	{
		// As a pawn, it can only go up or down depending on color,
		// EXTRA: Pawn cannot move backwards, so supposedly need to get a color check
		
		// Pawn only moves one forward and doesn't move horizontally
		if(owner()->get_component_type<Position>()->Get_column() != possible_move.column())
			return false;
		
		// The pawn is supposed to move only down and only one step ahead
		if(abs(owner()->get_component_type<Position>()->Get_row() - possible_move.row()) == 1)
			return true;

		return false;
	}
	
	
	
	
	
	
	
	// BishopMovement 
	BishopMovement::~BishopMovement(){}
	const char* BishopMovement::type() const {	return "BishopMovement";	}
	void BishopMovement::load(std::istream& is)	{ if(is == NULL) return; }
	void BishopMovement::save(std::ostream& os) const { if(os == NULL) return; }
	Component* BishopMovement::clone() const { return new BishopMovement(); }
	int BishopMovement::compare_to(const IComparable&) const { return 0; }
	
	// PawnMovement Component function to check the validity of the move
	bool BishopMovement::is_valid_move(BoardPosition possible_move) const
	{
		Position * bishop_pos = owner()->get_component_type<Position>();
		
		//TODO: READABILITY!
		// Diagonal = it moves as much vertically as it deos horizontally
		if(abs(bishop_pos->Get_row() - possible_move.row()) == abs(bishop_pos->Get_column() - possible_move.column()))
			return true;
		
		return false;
	}
	
	
	
	// RookMovement 
	RookMovement::~RookMovement(){}	
	const char* RookMovement::type() const {	return "RookMovement";	}
	void RookMovement::load(std::istream& is)	{ if(is == NULL) return; }
	void RookMovement::save(std::ostream& os) const { if(os == NULL) return; }
	Component* RookMovement::clone() const { return new RookMovement(); }
	int RookMovement::compare_to(const IComparable&) const { return 0; }
	
	// RookMovement Component function to check the validity of the move
	bool RookMovement::is_valid_move(BoardPosition possible_move) const
	{
		// Create a variable to store the Position of the rook
		Position * rook_pos = owner()->get_component_type<Position>();
		
		
		//TODO: READABILITY!
		// if it moves vertically, then it doesn't move horizontally
		if(rook_pos->Get_column() == possible_move.column())
			if(possible_move.row() > 0 && possible_move.row() < 9)
				return true;
		
		// if it moves horizontally, then it doesn't move vertically
		if(rook_pos->Get_row() == possible_move.row())
			if(possible_move.column() >= 'a' && possible_move.column() <= 'h')
				return true;
		
		return false;
	}
	
	// KnightMovement 
	KnightMovement::~KnightMovement(){}
	const char* KnightMovement::type() const {	return "KnightMovement";	}
	void KnightMovement::load(std::istream& is)	{ if(is == NULL) return; }
	void KnightMovement::save(std::ostream& os) const { if(os == NULL) return; }
	Component* KnightMovement::clone() const { return new KnightMovement(); }
	int KnightMovement::compare_to(const IComparable&) const { return 0; }
	
	// KnightMovement Component function to check the validity of the move
	bool KnightMovement::is_valid_move(BoardPosition possible_move) const
	{
		// Create a variable to store the Position of the rook
		Position * rook_pos = owner()->get_component_type<Position>();
		
		// TODO: SANITY CHECK THAT SATISFIES THE CONDITIONS
		if(possible_move.row() < 1 && possible_move.row() > 8 && possible_move.column() < 'a' && possible_move.column() > 'h')
			return false;
		
		//TODO: READABILITY!
		// if it moves vertically more than horizontally then row-wise it moved 2
		if(abs(rook_pos->Get_row() - possible_move.row()) == 2)
			// that also means that column-wise it moved 1 cell
			if(abs(rook_pos->Get_column() - possible_move.column()) == 1)
				return true;
		
		// if it moves horizontally more than vertically then column-wise it moved 2
		if(abs(rook_pos->Get_column() - possible_move.column()) == 2)
			// that also means that row-wise it moved 1 cell
			if(abs(rook_pos->Get_row() - possible_move.row()) == 1)
				return true;
		
		return false;
	}
	
	
	
	
	// KingMovement Implementation
	KingMovement::~KingMovement(){}
	const char* KingMovement::type() const {	return "KingMovement";	}
	void KingMovement::load(std::istream& is)	{ if(is == NULL) return; }
	void KingMovement::save(std::ostream& os) const { if(os == NULL) return; }
	Component* KingMovement::clone() const { return new KingMovement(); }
	int KingMovement::compare_to(const IComparable&) const { return 0; }
	
	// KingMovements Component function to check the validity of the move
	bool KingMovement::is_valid_move(BoardPosition possible_move) const
	{
		// Create a variable to store the Position of the rook
		Position * king_pos = owner()->get_component_type<Position>();
		
		// TODO: SANITY CHECK THAT SATISFIES THE CONDITIONS
		if(possible_move.row() < 1 && possible_move.row() > 8 && possible_move.column() < 'a' && possible_move.column() > 'h')
			return false;
		
		//TODO: READABILITY!
		// King can move horizontally, vertically and diagonally, but only one space
		if(king_pos->Get_row() == possible_move.row())
			if(abs(king_pos->Get_column() - possible_move.column()) == 1)
				return true;
		
		// Vertical only movement
		if(king_pos->Get_column() == possible_move.column())
			if(abs(king_pos->Get_row() - possible_move.row()) == 1)
				return true;
			
		// Diagonal Movement
		if((abs(king_pos->Get_column() - possible_move.column()) == 1) && (abs(king_pos->Get_row() - possible_move.row()) == 1))
			return true;
		
		return false;
	}
	
	
	
	
	
	
	
	
	// Queen Movement Component
	QueenMovement::~QueenMovement(){}
	const char* QueenMovement::type() const {	return "QueenMovement";	}
	void QueenMovement::load(std::istream& is)	{ if(is == NULL) return; }
	void QueenMovement::save(std::ostream& os) const { if(os == NULL) return; }
	Component* QueenMovement::clone() const { return new QueenMovement(); }
	int QueenMovement::compare_to(const IComparable&) const { return 0; }
	
	// KingMovements Component function to check the validity of the move
	bool QueenMovement::is_valid_move(BoardPosition possible_move) const
	{
		// Create a variable to store the Position of the Queen
		Position * queen_pos = owner()->get_component_type<Position>();
		
		// TODO: SANITY CHECK THAT SATISFIES THE CONDITIONS
		if(possible_move.row() < 1 && possible_move.row() > 8 && possible_move.column() < 'a' && possible_move.column() > 'h')
			return false;
		
		//TODO: READABILITY!
		// Queen can move horizontally, vertically and diagonally and as much as rook or bishop
		
		// Diagonal = it moves as much vertically as it deos horizontally
		if(abs(queen_pos->Get_row() - possible_move.row()) == abs(queen_pos->Get_column() - possible_move.column()))
			return true;
		
		// if it moves vertically, then it doesn't move horizontally
		if(queen_pos->Get_column() == possible_move.column())
			if(possible_move.row() > 0 && possible_move.row() < 9)
				return true;
		
		// if it moves horizontally, then it doesn't move vertically
		if(queen_pos->Get_row() == possible_move.row())
			if(possible_move.column() >= 'a' && possible_move.column() <= 'h')
				return true;
		
		return false;
	}
	
	
}	}	//namespace cs225::chess