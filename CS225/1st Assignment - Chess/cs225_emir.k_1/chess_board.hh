/**********************************************************************************/
/*!
\file   	chess_board.hh
\language 	C++
\author 	Emir Khanseitov
\par    	email: emir.k@digipen.edu
\par    	digiPen login: emir.k
\par    	Course: CS225
\par    	Assignment #1 - Chess
\date   	28/09/2018
\updated 	12/10/2018
\brief  
    Contains classes like Color, Board position and an exception class, as well as
	all necessary functions within them
        

\par Hours spent on this assignment: 
    30-40 hours, very desorganized hours


\par Specific portions that gave you the most trouble: 
    - Understanding concepts behind entity-component systems 
	- Managing the time around the assignment
	- Going straight on without stopping programming while coding the assignments
  
*/
/**********************************************************************************/

#pragma once

namespace cs225{ namespace chess{
	
	// COLOR CLASS
	class Color
	{
	public:
/*!****************************************************************************
 \fn	 color_getter() const
 
 \brief	 Function getter to get the current color

 \param  void

 \return int
******************************************************************************/
		int color_getter() const;
		
/*!****************************************************************************
 \fn	 color_setter(int other_color)
 
 \brief	 Function setter to set the current color to other_color

 \param  other_color
 int

 \return void
******************************************************************************/
		void color_setter(int other_color);
	
/*!****************************************************************************
 \fn	 operator== (int other_color) const
 
 \brief	 Operator overload for == check if white or black

 \param  other_color
 int

 \return bool
******************************************************************************/
		bool operator== (int other_color) const;
		
/*!****************************************************************************
 \fn	 operator== (Color other_color) const
 
 \brief	 Operator overload for == check if white or black

 \param  other_color
 Color

 \return bool
******************************************************************************/
		bool operator== (Color other_color) const;
	
		// Defines for which color is represented by what letter
		static const int white;
		static const int black;
		
	private:
		// thee color of the object -> starts out black
		int which;
	};
	
	
	
	// BOARDPOSITION CLASS
	class BoardPosition
	{
	public:
/*!****************************************************************************
 \fn	 BoardPosition()
 
 \brief	 Default Constructor to initialize the private members

 \param  void

 \return void
******************************************************************************/
		BoardPosition();
	
/*!****************************************************************************
 \fn	 BoardPosition()
 
 \brief	 Non-default Constructor to initialize the private members to parameters

 \param  Row
 int

 \param Column
 int

 \return void
******************************************************************************/
		BoardPosition(int Row, char Column);
		

/*!****************************************************************************
 \fn	 row() const
 
 \brief	 Getter functions for row 

 \param  void

 \return int
******************************************************************************/
		int row() const;
		
/*!****************************************************************************
 \fn	 column() const
 
 \brief	 Getter functions for column

 \param  void

 \return char
******************************************************************************/
		char column() const;
		
/*!****************************************************************************
 \fn	 set(int Row, char Column)
 
 \brief	 Setter function for row and column

 \param  Row
 int
 
 \param Column
 char

 \return void
******************************************************************************/
		void set(int Row, char Column);
		
/*!****************************************************************************
 \fn	 color() const
 
 \brief	 Function for determining color of the cell in the board

 \param  void

 \return Color
******************************************************************************/
		Color color() const;
		
/*!****************************************************************************
 \fn	 UpdateColor()
 
 \brief	 Function to update color based off the cell

 \param  void

 \return void
******************************************************************************/
		void UpdateColor();
		
	private:
		// Row and column members
		int m_rows;
		char m_columns;
		Color m_color;
		
	};
	
	
	
	
	
	
	// EXCEPTION CLASS
	class InvalidBoardPosition
	{
	public:
		// Constructor for Invalid class
/*!****************************************************************************
 \fn	 InvalidBoardPosition(int inv_Row, char inv_Column)
 
 \brief	 Non-default constructor for Invalid class

 \param  inv_Row
 int

 \param inv_Column
 int
 
 \return void
******************************************************************************/
		InvalidBoardPosition(int inv_Row, char inv_Column);
		
		// Getters of rows and columns
/*!****************************************************************************
 \fn	 row() const
 
 \brief	 Function getter to get number of m_inv_rows

 \param  void

 \return int
******************************************************************************/
		int row() const;
		
/*!****************************************************************************
 \fn	 column() const
 
 \brief	 Function getter to get number of m_inv_columns

 \param  void

 \return char
******************************************************************************/
		char column() const;
		
	private:
		// Row and column members
		int m_inv_rows;
		char m_inv_columns;
	};
		
		
		
} } // namespace cs225::chess