/**********************************************************************************/
/*!
\file   	chess_board.cc
\language 	C++
\author 	Emir Khanseitov
\par    	email: emir.k@digipen.edu
\par    	digiPen login: emir.k
\par    	Course: CS225
\par    	Assignment #1 - Chess
\date   	28/09/2018
\updated 	12/10/2018
\brief  
    Contains definitions of the functions in classes of BoardPosition and Color,
	as well the Exception class
        

\par Hours spent on this assignment: 
    30-40 hours, very desorganized hours


\par Specific portions that gave you the most trouble: 
    - Understanding concepts behind entity-component systems 
	- Managing the time around the assignment
	- Going straight on without stopping programming while coding the assignments
  
*/
/**********************************************************************************/
 
 
 
/*!****************************************************************************
 \fn	 Print_board()
 
 \brief	 Function that prints the board (And supposed to do pieces)

 \param  board_given
BoardPosition *
 
 \return void
******************************************************************************/