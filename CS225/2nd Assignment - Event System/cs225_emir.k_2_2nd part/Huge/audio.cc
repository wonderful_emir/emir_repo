#include "audio.hh"

#include <SDL/SDL_mixer.h>

#include <cassert>

namespace huge { namespace audio
{

namespace 
{

void check( ::Mix_Chunk* sound )
{
    if(sound == NULL)
    {
        const char* error = ::Mix_GetError();
        throw error;
    }
}

} // namespace 


Sound::Sound( const char* file )
    : filename(file)
    , sound( ::Mix_LoadWAV(filename) )
{
    check(sound);
}

Sound::Sound( const Sound& other )
    : filename(other.filename)
    , sound( ::Mix_LoadWAV(filename) )
{
    check(sound);
}

Sound::~Sound()
{
    ::Mix_FreeChunk(sound);
}

void Sound::play() const
{
    if( ::Mix_PlayChannel(-1, sound, 0) == -1 )
        throw "Cannot play sound";
}

} // namespace huge 
} // namespace audio
