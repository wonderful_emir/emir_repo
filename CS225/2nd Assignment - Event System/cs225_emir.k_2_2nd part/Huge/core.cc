#include "core.hh"

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_opengl.h>
#include <SDL/SDL_mixer.h>

#include <ctime>
#include <cassert>

#include <iostream> // std::cout, std::endl

namespace huge
{



Core::Core()
    : screen(NULL)
    , configuration()
{
    if( !init() )
        throw "Core init exception";
}

Core::~Core()
{
    cleanup();
}

Core::Configuration::Configuration()
    : window_width(1024)
    , window_height(768)
    , depth(32)
    , fullscreen(false)
{}

bool Core::init() try
{
    // seed for the random generator
    srand( std::time(0) );

    if( ::SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0 ) 
        throw "SDL initialization failed\n";

    // joystick support
#ifdef JOYSTICK_SUPPORT
    SDL_InitSubSystem( SDL_INIT_JOYSTICK );
    if( SDL_WasInit( SDL_INIT_JOYSTICK ) != 0 )
        throw "Joystick subsystem could not be initialized\n";
#endif

    screen = ::SDL_SetVideoMode( configuration.window_width, configuration.window_height, 
                                 SDL_GetVideoInfo()->vfmt->BitsPerPixel, 
                                 SDL_OPENGL | SDL_HWSURFACE | SDL_DOUBLEBUF );
    ::SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    // initialize SDL_image
    int img_formats = IMG_INIT_JPG | IMG_INIT_PNG; // add support for png and jpg formats
    if( (::IMG_Init( img_formats ) & img_formats) != img_formats ) 
        throw "Image support initialization failed\n";

    // initialize SDL_mixer 
    if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
        throw "Mixer initialization failed\n";

    return( screen && initGL() );

} catch( const char* err ) {
    std::cerr << err;
    return false;
}

bool Core::initGL()
{
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS,  1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES,  2);

    glShadeModel(GL_SMOOTH);
    //Set clear color
    glClearColor( 0, 0, 0, 0 );
    glClearDepth( 1.0f );
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    // Required if you want alpha-blended textures (for our fonts)
    glBlendFunc(GL_ONE, GL_ONE);
    glEnable(GL_BLEND);

    //Set projection
    glDisable( GL_DEPTH_TEST ) ;
    glViewport( 0, 0, configuration.window_width, configuration.window_height );
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    glOrtho( 0, configuration.window_width, configuration.window_height, 0, -1, 1 );
    //Initialize modelview matrix
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();
    // Check for errors
    return glGetError() == GL_NO_ERROR;
}

void Core::cleanup() 
{ 
    ::IMG_Quit();
    ::Mix_CloseAudio();
    ::SDL_Quit(); 
}

void Core::clear_screen()
{
    ::glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

void Core::render()
{
    ::SDL_Flip( screen );
    ::SDL_GL_SwapBuffers();
}

Core core;

unsigned int read_elapsed_ticks()
{
    return ::SDL_GetTicks();
}

void clear_screen()
{
    core.clear_screen();
}

void draw_frame()
{
    core.render();
}

} // namespace huge
