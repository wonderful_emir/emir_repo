#include "collision.hh"

namespace huge { namespace collision
{

AABB::AABB( geometry::Point ul, geometry::Point br )
    : rect( ul, br ) {}

AABB::AABB( geometry::Rect r )
    : rect( r ) {}

bool AABB::is_colliding( const AABB & other ) const
{
    return( ( ( rect.topleft.x <= other.rect.bottomright.x ) && ( other.rect.topleft.x <= rect.bottomright.x ) )
            &&
            ( ( rect.topleft.y <= other.rect.bottomright.y ) && ( other.rect.topleft.y <= rect.bottomright.y ) ) );
}

bool AABB::is_colliding( const geometry::Point & point ) const
{
    return( ( ( point.x >= rect.topleft.x ) && ( point.x <= rect.bottomright.x) )
            &&
            ( ( point.y >= rect.topleft.y ) && ( point.y <= rect.bottomright.y) ) );
}

} // namespace collision
} // namespace huge

