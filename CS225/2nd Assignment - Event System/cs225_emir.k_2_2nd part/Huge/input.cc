#include "input.hh"

#include <SDL/SDL.h>

#include <iostream> // just for debugging purposes

namespace huge { namespace input
{

/**************
 *  Keyboard  *
 **************/

Keyboard::Keyboard()
{}
Keyboard::Keyboard(const Keyboard&)
{}

Keyboard & keyboard()
{
    static Keyboard singleton_instance;
    return singleton_instance;
}

/**************
 *  Mouse  *
 **************/

Mouse::Mouse()
{}
Mouse::Mouse(const Mouse&)
{}

Mouse & mouse()
{
    static Mouse singleton_instance;
    return singleton_instance;
}

/*********************************
 *  SDL input handling routines  *
 *********************************/

void key_pressed( const SDL_keysym& keysym, bool state )
{
    switch( keysym.sym )
    {
        case SDLK_ESCAPE:   { keyboard().escape = state; break; }

        case SDLK_UP:       { keyboard().cursor.up = state; break; }
        case SDLK_DOWN:     { keyboard().cursor.down = state; break; }
        case SDLK_LEFT:     { keyboard().cursor.left = state; break; }
        case SDLK_RIGHT:    { keyboard().cursor.right = state; break; }

        case SDLK_w:        { keyboard().wasd.w = state; break;}
        case SDLK_a:        { keyboard().wasd.a = state; break;}
        case SDLK_s:        { keyboard().wasd.s = state; break;}
        case SDLK_d:        { keyboard().wasd.d = state; break;}

        default: { break; }
    }
}

void mouse_button_pressed()
{
    if( SDL_GetMouseState( &mouse().position.x, &mouse().position.y ) == SDL_BUTTON(1) )
    {
        mouse().button.left = true;
    }
    else if( SDL_GetMouseState( &mouse().position.x, &mouse().position.y ) == SDL_BUTTON(2) )
    {
        mouse().button.right = true;
    }
} 

} // namespace input

void handle_window_events()
{
    using input::key_pressed;
    using input::mouse_button_pressed;
    using input::mouse;

    SDL_Event event;

    while( SDL_PollEvent(&event) )
    {
        switch( event.type )
        {
            // window close
            case SDL_QUIT: 		{ break; }

            // Key pressed
            case SDL_KEYDOWN: 		{ key_pressed( event.key.keysym, true ); break; }
                                        
            // Key released
            case SDL_KEYUP: 		{ key_pressed( event.key.keysym, false ); break; }

            // Mouse button pressed
            case SDL_MOUSEBUTTONDOWN: 	{ mouse_button_pressed(); break; }

            // Mouse button released
            case SDL_MOUSEBUTTONUP: 	{ mouse().button.left = mouse().button.right = false; break; }

            // joystick events                                           
            case SDL_JOYBUTTONDOWN:     { std::cout << "Button pressed\n"; break; }
            case SDL_JOYAXISMOTION:     { std::cout << "Axis pressed\n"; break; }

            // Unknown event
            default:    		{ break; }
        }
    }
} 

} // namespace huge

