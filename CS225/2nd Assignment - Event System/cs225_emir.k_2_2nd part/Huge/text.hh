#pragma once

#include "geometry.hh"
#include "graphics.hh"

#include <SDL/SDL_ttf.h>      // manage true type fonts -> text display

#include <string>   // message storage for the Text class
#include <sstream>  // stringstream in the to_string function

struct SDL_Surface;

namespace huge { namespace graphics 
{

namespace 
{

// template function that converts generic data to std::string
template <typename T>
std::string to_string( const T& t )
{
    std::stringstream ss;
    ss << t;
    return ss.str();
}

} // namespace 

class Text 
{
public:
    // ------------------------------------------------
    // constructors / destructor / copy control

    Text( const std::string& str, const geometry::Rect frame, const graphics::Color txt_color = graphics::colors::white );
    ~Text();
    
    // initialize the static TTF font
    static bool init_font( const char* filename, unsigned int font_size );

    // ------------------------------------------------
    // message setting public functions

    // clears the contents of the message
    void clear_text();

    // accessor method for the message
    const std::string& get_text() const { return message; }
    std::string& get_text() { return message; }

    // assigns a single non-string parameter to the message
    template <typename T>
    void set_text( const T& t ) { message = to_string(t); }
    
    // ------------------------------------------------
    // public member functions
   
    /// updates the displayable texture with the current message
    void update(); 

    void draw() const;

private:

    // ------------------------------------------------
    // data members
    std::string     message; // the actual text message
    geometry::Rect  rect;    // rectangle that serves as a frame
    graphics::Color color;   // text color

    // internal data
    SDL_Surface* surface; // graphical representation of the text message
    unsigned int texture_id;

    static TTF_Font* font;

};

} // namespace huge
} // namespace graphics
