
#pragma once

#include <glm/glm.hpp>

namespace huge { namespace geometry
{

typedef glm::vec2 Point;

struct Line
{
    Line( Point origin, Point end );

    Point p0;
    Point p1;

}; // end of struct Line


struct Rect
{
    Rect( Point tl, Point br );

    Point topleft;
    Point bottomright;

}; // end of struct Rect


// ==================================
// Random geometry element generating
// ==================================

/// Create a random point on screen coordinates.
Point rand_point();

/// Create a line between two random points.
Line rand_line();

/// Create a random rectangle.
Rect rand_rect();

} // end of namespace Geometry
} // end of namespace huge

