#include "graphics.hh"

#if ( defined(__WIN32) || defined(_WIN64) || defined(_WIN32) )
#include <Windows.h>
#endif
#include <GL/gl.h>
#include <SDL/SDL_image.h>

#include <cassert>

namespace huge { namespace graphics
{

/***********
 *  Color  *
 ***********/

Color::Color( GLubyte _r, GLubyte _g, GLubyte _b, GLubyte _a )
    : r(_r), g(_g), b(_b), a(_a) 
{}

/***********
 *  Image  *
 ***********/

Image::Image( const std::string& filename )
    : id( Image::get_new_id() )
{
    SDL_Surface * image = IMG_Load( filename.c_str() ); 
    assert( image );
     
    glGenTextures( 1, &id );
    glBindTexture( GL_TEXTURE_2D, id );
     
    int mode = GL_RGB;
    if( image->format->BytesPerPixel == 4 ) 
        mode = GL_RGBA;
     
    glTexImage2D( GL_TEXTURE_2D, 0, mode, image->w, image->h, 0, mode, GL_UNSIGNED_BYTE, image->pixels );
     
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    SDL_FreeSurface( image ); 
}


/************
 *  Sprite  *
 ************/
    
Sprite::Sprite( const Image& img, geometry::Point pos )
    : position( pos ) 
    , width(30), height(30)
    , image_id( img.get_id() ) 
{}

void Sprite::draw() const 
{ 
    glEnable( GL_TEXTURE_2D );
    glBindTexture( GL_TEXTURE_2D, image_id );
     
    apply_view();
    glColor3f( 1.0f, 1.0f, 1.0f );
    glBegin( GL_QUADS );
        glTexCoord2f(0, 0); glVertex2f( position.x,          position.y );
        glTexCoord2f(1, 0); glVertex2f( position.x + width,  position.y );
        glTexCoord2f(1, 1); glVertex2f( position.x + width,  position.y + height );
        glTexCoord2f(0, 1); glVertex2f( position.x,          position.y + height );
    glEnd();
    glDisable( GL_TEXTURE_2D );
    glLoadIdentity();
}


/***********************
 *  Drawing functions  *
 ***********************/
    

void draw_point( const geometry::Point & p, const graphics::Color & color )
{
    apply_view();
    glBegin( GL_POINTS );
        glColor4f( color.r, color.g, color.b, color.a );
        glVertex2f( p.x, p.y );
    glEnd();
    glLoadIdentity();
}

void draw_fill_rect( const geometry::Point & tl, const geometry::Point & br, const graphics::Color & color )
{
    apply_view();
    glBegin( GL_QUADS );
        glColor4f( color.r, color.g, color.b, color.a );
        glVertex2f( tl.x, tl.y );
        glVertex2f( br.x, tl.y );
        glVertex2f( br.x, br.y );
        glVertex2f( tl.x, br.y );
    glEnd();
    glLoadIdentity();
}

void draw_fill_rect( const geometry::Rect & rect, const graphics::Color & color )
{ 
    draw_fill_rect( rect.topleft, rect.bottomright, color ); 
}

void draw_rect( const geometry::Point & tl, const geometry::Point & br, const graphics::Color & color )
{
    apply_view();
    glBegin( GL_LINE_LOOP );
        glColor4f( color.r, color.g, color.b, color.a );
        glVertex2f( tl.x, tl.y );
        glVertex2f( br.x, tl.y );
        glVertex2f( br.x, br.y );
        glVertex2f( tl.x, br.y );
    glEnd();
    glLoadIdentity();
}

void draw_rect( const geometry::Rect & rect, const graphics::Color & color )
{ 
    draw_rect( rect.topleft, rect.bottomright, color ); 
}

void draw_line( const glm::vec2 & p0, const glm::vec2 & p1, const graphics::Color & color )
{
    graphics::apply_view();
    glBegin( GL_LINES );
        glColor4f( color.r, color.g, color.b, color.a );
        glVertex2f( p0.x, p0.y );
        glVertex2f( p1.x, p1.y );
    glEnd();
    glLoadIdentity();
}

void draw_line( const geometry::Line & line, const graphics::Color & color )
{ 
    draw_line( line.p0, line.p1, color ); 
}

void apply_view()
{
    glTranslatef( view.translation.x, view.translation.y, 0.0f );
    glRotatef( view.rotation_angle, View::rotation_vec.x, View::rotation_vec.y, View::rotation_vec.z );
    glScalef( view.scale * View::scale_vec.x, view.scale * View::scale_vec.y, 1.0f );
}

View::View( glm::vec2 tr, float sc, float angle )
    : translation(tr) 
    , scale(sc)
    , rotation_angle(angle)
{}

/***************************
 *  Static initialization  *
 ***************************/

const Color colors::red    ( 0xFF, 0, 0, 0xFF );
const Color colors::green   ( 0, 0xFF, 0, 0xFF );
const Color colors::blue  ( 0, 0, 0xFF, 0xFF );
const Color colors::yellow ( 0, 0xFF, 0xFF, 0xFF );
const Color colors::black  ( 0, 0, 0, 0xFF );
const Color colors::white  ( 0xFF, 0xFF, 0xFF, 0xFF );

// initialize the camera
View view( glm::vec2(0.0f, 0.0f), 1.0f, 0.0f );
const glm::vec3 View::rotation_vec = glm::vec3(0.0f, 0.0f, 1.0f);
const glm::vec2 View::scale_vec = glm::vec2(1.0f, 1.0f);

} // namespace graphics
} // end of namespace huge
