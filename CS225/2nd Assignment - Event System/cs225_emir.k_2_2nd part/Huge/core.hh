
#pragma once

struct SDL_Surface;

namespace huge
{

class Core
{
public:

    Core();
    ~Core();

    /// Clear the screen.
    static void clear_screen();

    /// Draw a frame on the screen.
    void render();

private:
    /// Init video device, create a window and init image support.
    bool init();
    /// Initialize OpenGL.
    bool initGL();
    /// Shutdown the systems.
    static void cleanup();

    SDL_Surface* screen;

    /// Storage for runtime system configuration variables.
    struct Configuration
    {
        Configuration();

        unsigned int window_width;
        unsigned int window_height;
        unsigned int depth;

        bool fullscreen;

    } configuration;
};

extern Core core;

/// Read elapsed ticks since engine startup.
unsigned int read_elapsed_ticks();

void clear_screen();

void draw_frame();

} // namespace huge 

