#include "text.hh"

#if ( defined(__WIN32) || defined(_WIN64) || defined(_WIN32) )
#include <Windows.h>
#endif
#include <GL/gl.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>      // manage true type fonts -> text display



namespace huge { namespace graphics 
{

TTF_Font* Text::font = NULL;

Text::Text( const std::string & str, const geometry::Rect frame, const graphics::Color txt_color )
    :   message(str)
    ,   rect( frame )
    ,   color( txt_color )
{   
    // load the text font
    static bool has_ttf = graphics::Text::init_font( "data/font.ttf", 16u );
    assert(has_ttf);
    assert(Text::font);
}

Text::~Text()
{
    // cleanup the surface
    SDL_FreeSurface( surface );
    glDeleteTextures( 1, &texture_id );
}

bool Text::init_font( const char * filename, unsigned int font_size )
{
    // initialize TTF
    if( TTF_Init() == -1 ) return false;
        
    font = TTF_OpenFont( filename, font_size );

    // check for font file loading success
    return (font != NULL);
}

void Text::clear_text() 
{ 
    message.clear(); 
}

void Text::update() // updates the displayable texture with the current message
{
    // create a software texture from the text
	SDL_Color color = {255,255,255};
    surface = TTF_RenderText_Solid( Text::font, message.c_str(), color );
    //assert( surface );
    // create the hardware texture from the surface
    //texture = SDL_CreateTextureFromSurface( Screen::renderer, surface);

    SDL_Surface * intermediary = SDL_CreateRGBSurface(0, surface->w, surface->h, 32, 
            0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);

    SDL_BlitSurface( surface, 0, intermediary, 0 );
    
    // Tell GL about our new texture 
    glGenTextures( 1, &texture_id );
    glBindTexture( GL_TEXTURE_2D, texture_id );
    // glTexImage2D( GL_TEXTURE_2D, 0, 4, surface->w, surface->h, 0, GL_BGRA, 
	glTexImage2D( GL_TEXTURE_2D, 0, 4, surface->w, surface->h, 0, GL_RGBA, 
            GL_UNSIGNED_BYTE, intermediary->pixels );
    
    // GL_NEAREST looks horrible, if scaled... 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);   

    SDL_FreeSurface( intermediary );
}

void Text::draw() const
{
    glEnable( GL_TEXTURE_2D );
    glBindTexture( GL_TEXTURE_2D, texture_id );
     
    graphics::apply_view();
    glColor3f( 1.0f, 1.0f, 1.0f );
    glBegin( GL_QUADS );
        glTexCoord2f(0, 0); glVertex2f( rect.topleft.x,      rect.topleft.y );
        glTexCoord2f(1, 0); glVertex2f( rect.bottomright.x,  rect.topleft.y );
        glTexCoord2f(1, 1); glVertex2f( rect.bottomright.x,  rect.bottomright.y );
        glTexCoord2f(0, 1); glVertex2f( rect.topleft.x,      rect.bottomright.y );
    glEnd();
    glLoadIdentity();
    glDisable( GL_TEXTURE_2D );
}

} // namespace huge
} // namespace graphics
