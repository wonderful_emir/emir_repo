
#pragma once

#include "geometry.hh"

namespace huge { namespace collision
{

/// Axis-alligned bounding box.
struct AABB
{
    AABB( geometry::Point ul, geometry::Point br );
    explicit AABB( geometry::Rect r );


    /// Check collision with another AABB
    bool is_colliding( const AABB & other ) const;

    /// Check collision with a point
    bool is_colliding( const geometry::Point & point ) const;

    geometry::Rect rect;
};

} // namespace collision
} // namespace huge


