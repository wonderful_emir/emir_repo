#pragma once

struct Mix_Chunk;

namespace huge { namespace audio
{

class Sound 
{
public:
    /// Load a sound from a wav file
    explicit Sound( const char* file );
    Sound( const Sound& other );
    ~Sound();

    /// Play a loaded sound (once)
    void play() const;

private:
    const char* filename;
    ::Mix_Chunk * sound;

}; // Sound


} // namespace audio
} // namespace huge

