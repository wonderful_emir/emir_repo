#pragma once


#include "geometry.hh"

#include <glm/glm.hpp>

#include <string>

// color indexes
enum { red, green, blue, yellow, black, white };

namespace huge { namespace graphics
{

struct Color
{
    Color( unsigned char _r, unsigned char _g, unsigned char _b, unsigned char _a );

    unsigned char r, g, b, a;

}; // struct Color

// well known color constants
struct colors
{
    static const Color red;
    static const Color blue;
    static const Color green;
    static const Color yellow;
    static const Color black;
    static const Color white;
};

class Image
{
public:
    explicit Image( const std::string& filename );

    unsigned int get_id() const { return id; }

private:
    static unsigned int get_new_id()
    {
        static unsigned int current_id = 0;
        return current_id++;
    }

    unsigned int  id;

}; // end of class Image

class Sprite
{
public:
    Sprite( const Image& img, geometry::Point pos );

    void draw() const;

    geometry::Point position;
    int             width, height;

private:
    unsigned int    image_id;

}; // end of class Sprite


/***********************
 *  Drawing functions  *
 ***********************/

void draw_point( const geometry::Point & p, const Color & color = graphics::colors::white );

void draw_fill_rect( const geometry::Point & tl, const geometry::Point & br, const graphics::Color & color = graphics::colors::white );

void draw_fill_rect( const geometry::Rect & rect, const Color & color = graphics::colors::white ); 

void draw_rect( const geometry::Point & tl, const geometry::Point & br, const graphics::Color & color = graphics::colors::white );

void draw_rect( const geometry::Rect & rect, const Color & color = graphics::colors::white );

void draw_line( const glm::vec2 & p0, const glm::vec2 & p1, const graphics::Color & color = graphics::colors::white );

void draw_line( const geometry::Line & line, const Color & color = graphics::colors::white );


void apply_view();

// camera object
struct View
{
    static const glm::vec3 rotation_vec;
    static const glm::vec2 scale_vec;

    View( glm::vec2 tr, float sc, float angle  );

    glm::vec2 translation;
    float scale;
    float rotation_angle;

};

extern View view;

} // end of namespace graphics
} // end of namespace huge


