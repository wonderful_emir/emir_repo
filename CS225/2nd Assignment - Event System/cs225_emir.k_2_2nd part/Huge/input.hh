#pragma once

struct SDL_keysym;

namespace huge { namespace input
{

struct Keyboard
{

    struct { bool up, down, left, right; } cursor;
    struct { bool w, a, s, d; } wasd;

    bool escape;

    friend Keyboard & keyboard(); // only singleton function can access private constructors

private:
    Keyboard();
    Keyboard(const Keyboard&);
};

// retrieve a singleton instance for Keyboard
Keyboard & keyboard();


struct Mouse
{
    struct { int x, y; } position;
    struct { bool left, right; } button;

    friend Mouse & mouse(); // only singleton function can access private constructors
private:
    Mouse();
    Mouse(const Mouse&);
};

// retrieve a singleton instance for Mouse
Mouse & mouse();

// Function that handles keyboard input based on the captured event in the event loop
// Key definition reference: http://www.libsdl.org/cgi/docwiki.cgi/SDLKey
void key_pressed( const SDL_keysym& keysym, bool state );

// Handle a mouse button press
void mouse_button_pressed();


} // namespace input

// Handle window events
void handle_window_events();

} // namespace huge

