#include "paddle.hh"

using namespace huge;

/***********************************
 *  Paddle method implementations  *
 ***********************************/

<<<<<<< HEAD
=======
 // Use the instance and flags from Emirs_events 
cs225::EventDispatcher & event_dispatcher = cs225::EventDispatcher::get_instance();
bool f_Bound_Paddle = false;

>>>>>>> master
Paddle::Paddle( huge::geometry::Point initial_pos )
    : position(initial_pos)
    , box( geometry::Point(0, 0), geometry::Point(30, 80) )
    , velocity(0.0f)
{}

void Paddle::update( float dt )
{
<<<<<<< HEAD
    position.y -= velocity * dt;

    // TODO: students should complete/modify this as they see fit
	
	// Conditionals -> upon collision start increasing the position by velocity, so depends on collisions
=======
	if (f_Bound_Paddle)
	{
		// For now
		position.y = position.y - 1;
		f_Bound_Paddle = false;
	}
	else
		position.y -= velocity * dt;
>>>>>>> master
}

void Paddle::draw() const
{
    // draw the paddle as a blue rectangle
    geometry::Rect display( box.rect.topleft + position, box.rect.bottomright + position );
    graphics::draw_fill_rect( display, graphics::colors::blue );
}

bool Paddle::check_collision( const Solid& other ) const
{
<<<<<<< HEAD
	// Event based -> collision of paddle with:
	// WALLS at TOP and BOTTOM <- cannot go past WALLS, and other paddle changes position +=
	// BALL -> and it needs to bounce in another direction

    // TODO: students should complete/modify this as they see fit
    return false;
=======
	// with this I can get type of Solid that is passed to me
	if (cs225::type_of(other).get_name() == cs225::type_of(PlayingField()).get_name())
	{
		// Dynamic Cast to working with PlayingField
		const PlayingField * my_PlayingField = dynamic_cast<const PlayingField *>(&other);

		// If there was a collision with upper wall or lower wall -> trigger the event and return collision true
		if (!my_PlayingField->upper_wall.is_colliding(box) || !my_PlayingField->lower_wall.is_colliding(box))
		{
			// trigger an event of Ball colliding with the paddle
			event_dispatcher.trigger_event(Events::Bound_Paddle_collision_event());

			return true;
		}
			
	}

	// if there wasn't any collision -> return nothing
	return false;
}

// handle_event fulfills function dependant on what is the collision with
void Paddle::handle_event(const cs225::Event & event_given)
{
	// Make the paddle do a different thing
	if (cs225::type_of(event_given).get_name() == cs225::type_of(Events::Bound_Paddle_collision_event()).get_name())
		f_Bound_Paddle = true;
>>>>>>> master
}
