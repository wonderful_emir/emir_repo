/*! \file assets.hh
 *  Game asset storage utilities.
 * 
 *  This file provides a centralized repository for all game assets.
 *  At the moment game assets are just images and sounds.
 *  All resources have a named index that is manually set to describe the 
 *  element being stored. E.g. the file "blip.wav" is stored at an index that
 *  matches the value of the enum 'blip' in order to access elements by descriptive id.
 *
 *  Images are currently not being used (but usable).
 */
 
#pragma once

// forward declarations
namespace huge { 
namespace graphics {
    class Image;
} // namespace huge 
namespace audio {
    class Sound;
} // namespace audio
} // namespace huge 

// indices for image resources
// used to access the image container in a more meaningful way: images[grass]
// needs to be manually set up so the identifiers match the element index (flare is the 0-th element, etc.)
enum { flare, grass, mario, totem };
// enum indices for the sound asset collection
enum { blip, blop, goal };

struct Assets
{
    /// Retrieve an image resource with descriptive id @c id.
    static const huge::graphics::Image& get_image( int id );
    /// Retrieve a sound resource with descriptive id @c id.
    static const huge::audio::Sound& get_sound( int id );
};
