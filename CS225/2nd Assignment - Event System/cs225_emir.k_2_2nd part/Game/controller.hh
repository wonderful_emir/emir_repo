#pragma once

#include "common_interfaces.hh"

class Paddle; // forward declaration

class Controller
{
protected:
    Controller( Paddle& pad );

    /// Update the controlled paddle state
    virtual void control() = 0;

    /// Paddle being controlled by this controller
    Paddle& paddle;
};

/// A sample concrete controller that does nothing remarkable
class DummyController : public Dynamic, public Controller
{
public:
    DummyController( Paddle& pad );

    void update(float);

    void control();

}; // DummyController

/// Entity that represents the control that a player applies on a paddle
class PlayerController : public Dynamic, public Controller 
{
public:
    PlayerController( Paddle& pad );

	void update(float);

    void control();

}; // PlayerController 
