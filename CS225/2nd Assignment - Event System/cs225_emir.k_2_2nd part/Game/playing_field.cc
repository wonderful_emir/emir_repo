#include "playing_field.hh"

using namespace huge;

/******************************************
 *  Playing field method implementations  *
 ******************************************/


PlayingField::PlayingField()
    : upper_wall( geometry::Point(0, 0), geometry::Point(1024, 100) )
    , lower_wall( geometry::Point(0, 668), geometry::Point(1024, 768) )
    , left_goal( geometry::Point(16, 100), geometry::Point(20, 668) )
    , right_goal( geometry::Point(1004, 100), geometry::Point(1008, 668) )
{}

void PlayingField::draw() const
{
    // draw the walls as red blocks
    graphics::draw_fill_rect( upper_wall.rect, graphics::colors::red );
    graphics::draw_fill_rect( lower_wall.rect, graphics::colors::red );

    // draw the goals as green rectangles
    graphics::draw_rect( left_goal.rect.topleft, left_goal.rect.bottomright, graphics::colors::green );
    graphics::draw_rect( right_goal.rect.topleft, right_goal.rect.bottomright, graphics::colors::green );
}

bool PlayingField::check_collision( const Solid& other ) const
{
<<<<<<< HEAD
	// TODO
    return false;


	// Also if Goal -> score increase(), if received a goal -> score decrease
=======
	// To not double check -> we won't check here with ball or paddle for collision
    return false;
>>>>>>> master
}

/**********************************
 *  Score method implementations  *
 **********************************/

Score::Score()
    : displayed_score( "", geometry::Rect(geometry::Point(500, 30), geometry::Point(550, 100)) )
    , value(0u)
{
    displayed_score.set_text(value);
    displayed_score.update();
}

void Score::increase()
{
    value++;
    displayed_score.set_text(value);
    displayed_score.update();
}

void Score::decrease()
{
	value--;
	displayed_score.set_text(value);
	displayed_score.update();
}

void Score::draw() const
{
    displayed_score.draw();
}

