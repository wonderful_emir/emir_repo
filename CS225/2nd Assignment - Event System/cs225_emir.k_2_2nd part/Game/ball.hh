/*! Ball game object declarations.
 * 
 *  This file contains the declarations for the game object that
 *  implements the ball in the game.
 */
 
#pragma once

#include "huge.hh"              // engine includes
<<<<<<< HEAD
#include "common_interfaces.hh" // game interfaces 

class Ball : public Dynamic, public Drawable, public Solid
=======
#include "common_interfaces.hh" // game interfaces

#include "paddle.hh"			// to work with paddle and PlayingField

class Ball : public Dynamic, public Drawable, public Solid, public cs225::Listener
>>>>>>> master
{
public:
    /// Spawn a ball at a given position
    Ball( huge::geometry::Point initial_pos );

    // draw the ball
    void draw() const;

    // updates the ball state every tick
    void update( float dt );

    // collision-checking specialization
    bool check_collision( const Solid& other ) const;
<<<<<<< HEAD
=======

	//
	virtual void handle_event(const cs225::Event & event_given);
>>>>>>> master
    
private:
    huge::geometry::Point  position;   // player coordinates
    huge::collision::AABB  box;        // bounding box to check for collisions
    glm::vec2              velocity;   // moving velocity vector

}; // Ball

