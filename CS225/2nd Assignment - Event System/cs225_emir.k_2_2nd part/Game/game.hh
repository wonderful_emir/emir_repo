/*! Game-specific declarations.
 * 
 *  This file contains the high level declarations for the 
 *  game contents.
 */
 

#pragma once

#include "huge.hh"              // include the engine
#include "common_interfaces.hh" // game interfaces

// game element includes
#include "playing_field.hh"
#include "ball.hh"
#include "paddle.hh"
#include "controller.hh"

#include <vector>

class Game
{
public:
    Game();

    /// Game update function
    void tick(float dt);

<<<<<<< HEAD
=======
	// Getter of the ball and the left_paddle
	const Ball	 & get_ball() const;
	const Paddle & get_left_paddle() const;
	const Paddle & get_right_paddle() const;

>>>>>>> master

private:
    /// Updateable elements
    std::vector<Dynamic*>  dynamic_elements;
    /// Drawable elements
    std::vector<Drawable*> drawable_elements;
    /// Collidable elements
    std::vector<Solid*>    solid_elements;

    // Game objects ---------------------------
    /// The playing field object
    PlayingField field;
    /// The score display
    Score score;

    /// The ball
    Ball ball;

    /// The player paddles
    Paddle left_paddle;
    Paddle right_paddle;

    /// The controller entities that will control the paddle behavior
    PlayerController player_controller;
    DummyController dummy_controller;
};

