/*! Game implementation.
 * 
 *  This file contains the high level entry functions for the game.
 */
 
#include "game.hh"
#include "assets.hh"

#include <iostream>
using namespace huge;


/*********************************
 *  Game method implementations  *
 *********************************/

Game::Game()
    : dynamic_elements()
    , drawable_elements()
    , solid_elements()
    , field()
    , score()
    , ball( geometry::Point(500, 400) )
    , left_paddle( geometry::Point(100, 200) )
    , right_paddle( geometry::Point(800, 200) )
    , player_controller(left_paddle)
    , dummy_controller(right_paddle)
{
    // force resource initialization to avoid
    // loading at runtime
    Assets::get_sound(0);
    Assets::get_image(0);

    // subscribe the updateable elements
    dynamic_elements.push_back( &ball );
    dynamic_elements.push_back( &left_paddle );
	dynamic_elements.push_back( &player_controller);
    dynamic_elements.push_back( &right_paddle );
    dynamic_elements.push_back( &dummy_controller );

    // subscribe the drawable elements
    drawable_elements.push_back( &field );
    drawable_elements.push_back( &score );
    drawable_elements.push_back( &ball );
    drawable_elements.push_back( &left_paddle );
    drawable_elements.push_back( &right_paddle );

    // subscribe the collidable elements
    solid_elements.push_back( &ball );
    solid_elements.push_back( &field );
    solid_elements.push_back( &left_paddle );
    solid_elements.push_back( &right_paddle );

}

void update_game_objects( std::vector<Dynamic*> & updateables, float dt )
{
    for( std::vector<Dynamic*>::iterator it = updateables.begin(); it != updateables.end(); ++it )
        (*it)->update(dt);
}

void draw_scene( const std::vector<Drawable*> & drawables )
{
    for( std::vector<Drawable*>::const_iterator it = drawables.begin(); it != drawables.end(); ++it )
        (*it)->draw();
}

void check_collisions( const std::vector<Solid*> & collidables )
{
    // this is an N^2 (check all vs all) naive collision test
    // caveat: with this approach every collision will be checked twice
    // given 2 objects A and B there will be a check as A.check_collision(B) and another as B.check_collision(A)
    for( std::vector<Solid*>::const_iterator checker_it = collidables.begin(); checker_it != collidables.end(); ++checker_it )
        for( std::vector<Solid*>::const_iterator to_check_it = collidables.begin(); to_check_it != collidables.end(); ++to_check_it )
        {
            if( checker_it != to_check_it ) // avoid checking a solid against itself
                (*checker_it)->check_collision(*(*to_check_it));
        }
}

void Game::tick(float dt)
{
    check_collisions(solid_elements);
    update_game_objects(dynamic_elements, dt);
    draw_scene(drawable_elements);
}

<<<<<<< HEAD
=======
const Ball & Game::get_ball() const
{
	return ball;
}

const Paddle & Game::get_left_paddle() const
{
	return left_paddle;
}

const Paddle & Game::get_right_paddle() const
{
	return right_paddle;
}

>>>>>>> master
