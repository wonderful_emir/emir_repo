/*! Ball game object definitions.
 * 
 *  This file contains the implementation for the 
 *  game object that implements the ball in the game.
 */

#include "ball.hh"
<<<<<<< HEAD
=======
#include <iostream>
>>>>>>> master

using namespace huge;

/*********************************
 *  Ball method implementations  *
 *********************************/

<<<<<<< HEAD
=======
 // Use the instance and flags from Emirs_events 
cs225::EventDispatcher & event_dispatcher = cs225::EventDispatcher::get_instance();

bool f_Ball_Paddle = false;
bool f_Bound_Ball = false;

bool f_Ball_MyGoal = false;
bool f_Ball_TheirGoal = false;



>>>>>>> master
Ball::Ball( geometry::Point initial_pos )
    : position(initial_pos)
    , box( geometry::Point(0, 0), geometry::Point(30, 30) )
    , velocity(220, 220)
{}

void Ball::draw() const
{
    // draw the ball as a white filled rectangle
    geometry::Rect display( box.rect.topleft + position, box.rect.bottomright + position );
    graphics::draw_fill_rect( display, graphics::colors::white );
}

void Ball::update( float dt )
{
<<<<<<< HEAD
=======
	// Conditional because of which the Velocity of ball changes
	if (f_Ball_Paddle)
	{
		velocity.x *= -1.2f;
		f_Ball_Paddle = false;
	}
	else if (f_Bound_Ball)
	{
		velocity.y *= -1.0f;
		f_Bound_Ball = false;
	}
		
>>>>>>> master
    const glm::vec2 movement = velocity * dt;
    position += movement;
}

bool Ball::check_collision( const Solid& other ) const
{
<<<<<<< HEAD
	// TODO
    // stub implementation
    // students may change this
    return false;
}
=======
	// with this I can get type of Solid that is passed to me
	if (cs225::type_of(other).get_name() == cs225::type_of(Paddle(huge::geometry::Point(0,0))).get_name())
	{
		// debug
		//std::cout << cs225::type_of(Paddle(huge::geometry::Point(0, 0))).get_name() << std::endl;
		
		// Dynamic Cast to working with Paddle
		const Paddle * my_Paddle = dynamic_cast<const Paddle *>(&other);

		// if there was a collision -> then trigger the event and return true
		if (my_Paddle->box.is_colliding(this->box))
		{
			// trigger an event of Ball colliding with the paddle
			event_dispatcher.trigger_event(Events::Ball_Paddle_collision_event());

			return true;
		}	
	}


	if (cs225::type_of(other).get_name() == cs225::type_of(PlayingField()).get_name())
	{
		// Dynamic Cast to working with PlayingField
		const PlayingField * my_PlayingField = dynamic_cast<const PlayingField *>(&other);
		
		// if there was a collision with upper wall or lower wall -> trigger event and return true 
		if (my_PlayingField->upper_wall.is_colliding(box) || my_PlayingField->lower_wall.is_colliding(box))
		{
			// trigger an event of Ball colliding with the Boundaries
			event_dispatcher.trigger_event(Events::Bound_Ball_collision_event());

			return true;
		}

		// otherwise there's a goal either to mine or opponents, so trigger appropriate event
		else if (my_PlayingField->left_goal.is_colliding(box))
		{
			// trigger an event of losing a point
			event_dispatcher.trigger_event(Events::Ball_My_Goal_collision_event());

			return true;
		}

		else if (my_PlayingField->right_goal.is_colliding(box))
		{
			// trigger an event of winning a point
			event_dispatcher.trigger_event(Events::Ball_Their_Goal_collision_event());

			return true;
		}
	}
		
	// if there wasn't any collision -> return nothing
	return false;
}


// handle_event fulfills function dependant on what is the collision with
void Ball::handle_event(const cs225::Event & event_given)
{
	// Make the ball do a different thing
	if (cs225::type_of(event_given).get_name() == cs225::type_of(Events::Ball_Paddle_collision_event()).get_name())
		f_Ball_Paddle = true;

	else if (cs225::type_of(event_given).get_name() == cs225::type_of(Events::Ball_My_Goal_collision_event()).get_name())
		f_Ball_MyGoal = true;

	else if (cs225::type_of(event_given).get_name() == cs225::type_of(Events::Ball_Their_Goal_collision_event()).get_name())
		f_Ball_TheirGoal = true;

	else if (cs225::type_of(event_given).get_name() == cs225::type_of(Events::Bound_Ball_collision_event()).get_name())
		f_Bound_Ball = true;
}
>>>>>>> master
