/**********************************************************************************/
/*!
\file   	type_info.cc
\language 	C++
\author 	Emir Khanseitov
\par    	email: emir.k@digipen.edu
\par    	digiPen login: emir.k
\par    	Course: CS225
\par    	Assignment #2 - Event System
\date   	13/10/2018
\updated 	16/10/2018
\brief  
    This file contains definitions of functions that are used within TypeInfo 
    class
        

\par Hours spent on this assignment: 
    8


\par Specific portions that gave you the most trouble: 
    - the fact that std::maps needed operator< overload
    - that we didn't need to build our own typeid() operator
    - I kind of forgot at one point how to turn a variable to a pointer... by &
    - Logic about Events has still left me wondering how it works
    - The way Multimap works, I know that my method may be not the most optimized
      when I loop
  
*/
/**********************************************************************************/

#include "type_info.hh"

namespace cs225 {

		// Conversion Constructor
		TypeInfo::TypeInfo(const std::type_info& rhs) : m_typeInfo(rhs) {}
			

		// Function Getter of the name
		const char* TypeInfo::get_name() const { return m_typeInfo.name(); }

		// Operator overload of == to Compare with other TypeInfos
		bool TypeInfo::operator==(const TypeInfo& rhs) const
		{
			return (m_typeInfo == rhs.m_typeInfo);
		}

		// Operator overload of != to Compare with other TypeInfos
		bool TypeInfo::operator!=(const TypeInfo& rhs) const
		{
			return (m_typeInfo != rhs.m_typeInfo);
		}

		// Necessary operator overload of <, for std::maps to compare/put in right place 
		bool TypeInfo::operator<(const TypeInfo & rhs) const
		{
			return m_typeInfo.before(rhs.m_typeInfo);
		}

} // namespace cs225