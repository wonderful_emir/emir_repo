#include "assets.hh"
#include "huge.hh" // engine declarations
using namespace huge;

#include <vector>

/****************************************
 *  Game assets method implementations  *
 ****************************************/

// Sound loading helper
std::vector<audio::Sound> load_sounds()
{
    std::vector<audio::Sound> loading;

    // manually populate the sound collection
    loading.push_back( audio::Sound("data/blip.wav") );
    loading.push_back( audio::Sound("data/blop.wav") );
    loading.push_back( audio::Sound("data/goal.wav") );

    return loading;
}

// Image loading helper
std::vector<graphics::Image> load_images()
{
    std::vector<graphics::Image> loading;
    // add the image files to be used
    // remember to change the enum above accordingly so the images container can be used with meaningful identifiers
    loading.push_back( graphics::Image("data/flare.png") );
    loading.push_back( graphics::Image("data/grass.png") );
    loading.push_back( graphics::Image("data/mario.jpg") );
    loading.push_back( graphics::Image("data/totem.png") );

    return loading;
}

// Singleton-like accessors ------------

const graphics::Image& Assets::get_image( int id )
{
    static std::vector<huge::graphics::Image> images = load_images();
    return images.at(id); // will throw if out of range
}

const audio::Sound& Assets::get_sound( int id )
{
    static std::vector<huge::audio::Sound> sounds = load_sounds();
    return sounds.at(id); // will throw if out of range
}

