#pragma once

// Include the event dispatcher onto your game
#include "event_dispatcher.hh"

namespace Events {
	// Get new event_types -> for ball, Boundary and Input
	struct Ball_Paddle_collision_event : public cs225::Event {};
	struct Ball_My_Goal_collision_event : public cs225::Event {};
	struct Ball_Their_Goal_collision_event : public cs225::Event {};

	struct Bound_Paddle_collision_event : public cs225::Event {};
	struct Bound_Ball_collision_event : public cs225::Event {};

	struct Input_UP_given_event : public cs225::Event {};
	struct Input_DOWN_given_event : public cs225::Event {};
}

// Structure of events possible


// MemberHandlerFunction as something related to collision itself, as well as Event Handler

struct Input_listener : public cs225::Listener
{ virtual void handle_event(const cs225::Event & event_given); };




