#include "huge.hh" // huge engine
#include "game.hh" // class Game
<<<<<<< HEAD
=======
#include <iostream>
>>>>>>> master

// this are hacks that *should* fix any problems caused by differences
// in visual studio versions
#if defined(_MSC_VER) && (_MSC_VER >= 1900)
#pragma comment(lib, "legacy_stdio_definitions.lib")
#endif

#ifdef main
#undef main
#endif

<<<<<<< HEAD
=======


// Use the instance from Emirs_events
extern cs225::EventDispatcher & event_dispatcher;

// Now my listeners need to have an appropriate handle func()
// Also they would trigger whoever is subscribed to the event


>>>>>>> master
int main(int argc, char* args[])
{
    using namespace huge;

<<<<<<< HEAD
	// TODO -> Implement this possibly in another file or function of sorts
	cs225::EventDispatcher & event_dispatcher = cs225::EventDispatcher::get_instance();

	// Get new event_types -> for ball, then for paddle
	struct Ball_Bounds_collision_event	: public cs225::Event {};
	struct Ball_Goal_collision_event	: public cs225::Event {};
	struct Ball_Paddle_collision_event	: public cs225::Event {};

	struct Paddle_Bound_collision_event : public cs225::Event {};
	struct Input_given_event			: public cs225::Event {};


	Ball_Bounds_collision_event		b_b_event; 
	Ball_Goal_collision_event		b_g_event;
	Ball_Paddle_collision_event		b_p_event;

	Paddle_Bound_collision_event	p_b_event;
	Input_given_event				input_event;

	// Get new listeners -> Paddle listener and Ball listener
	struct Ball_listener : public cs225::Listener
	{
		// Default Constructor
		//Ball_listener() 
		//: f_ball_boundary_collide(false), f_ball_goal_collide(false), f_ball_paddle_collide(false)
		//{}

		// handle_event fulfills function dependant on what is the collision with
		virtual void handle_event(const cs225::Event & event_given)
		{
			// Conditionals that shall trigger an appropriate function 
			// if an appropriate event is passed
			if (event_given == &b_b_event)
				;// do ball with bounds collision
			else if (event_given == &b_g_event)
				; // do ball with goals collision
			else
				; // do ball with paddle collision
		}

		// Flags for with what the collision is with
		//bool f_ball_boundary_collide;
		//bool f_ball_goal_collide;
		//bool f_ball_paddle_collide;

	};

    Game game;

=======
	

	//event_dispatcher.subscribe(input_listener, cs225::type_of<Events::Input_UP_given_event>());
	//event_dispatcher.subscribe(input_listener, cs225::type_of<Events::Input_DOWN_given_event>());

    Game game;

	// Subscribe the listeners to appropriate Event Types
	event_dispatcher.subscribe(const_cast<Ball&>(game.get_ball()), cs225::type_of<Events::Ball_Paddle_collision_event>());
	event_dispatcher.subscribe(const_cast<Ball&>(game.get_ball()), cs225::type_of<Events::Ball_My_Goal_collision_event>());
	event_dispatcher.subscribe(const_cast<Ball&>(game.get_ball()), cs225::type_of<Events::Ball_Their_Goal_collision_event>());
	event_dispatcher.subscribe(const_cast<Ball&>(game.get_ball()), cs225::type_of<Events::Bound_Ball_collision_event>());


	event_dispatcher.subscribe(const_cast<Paddle&>(game.get_left_paddle()), cs225::type_of<Events::Bound_Paddle_collision_event>());

>>>>>>> master
    while( !input::keyboard().escape )
    {
        static unsigned int current_ticks = 0, prev_frame = read_elapsed_ticks(), elapsed_ticks;

        // read elapsed time since the previous iteration
        current_ticks = read_elapsed_ticks();
        elapsed_ticks = current_ticks - prev_frame;
        static float dt;
        prev_frame = current_ticks;
        dt = static_cast<float>( elapsed_ticks ) / 1000.0f;

        // read window events, input, etc.
        handle_window_events();

        clear_screen();

        game.tick(dt);

        draw_frame();
    }

	return 0;
}
