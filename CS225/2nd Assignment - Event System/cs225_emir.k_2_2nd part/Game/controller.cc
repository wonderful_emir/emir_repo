#include "controller.hh"

#include "huge.hh"
using namespace huge;

#include "paddle.hh"

/***************************************
 *  Controller method implementations  *
 ***************************************/

Controller::Controller( Paddle& pad )
    : paddle(pad)
{}


/************************************
 *  DummyController implementation  *
 ************************************/

DummyController::DummyController( Paddle& pad )
    : Controller(pad)
{}


void DummyController::update(float)
{
    control();
}

void DummyController::control()
{
    // be dummy
    paddle.velocity += (std::rand() % 10) - 5;
}

/*************************************
 *  PlayerController implementation  *
 *************************************/


PlayerController::PlayerController( Paddle& pad )
    : Controller(pad)
{}

void PlayerController::update(float)
{
	control();
}

void PlayerController::control()
{
	// Move your paddle as you see fit, up or down
	if (huge::input::keyboard().wasd.w == true)
		paddle.position.y -= 4;
	else if (huge::input::keyboard().wasd.s == true)
		paddle.position.y += 4;
}
