#pragma once

<<<<<<< HEAD
#include "huge.hh" // engine includes
#include "common_interfaces.hh" // game interfaces 

class Paddle :  public Dynamic, public Drawable, public Solid
=======
#include "huge.hh"				// engine includes
#include "common_interfaces.hh" // game interfaces 
#include "playing_field.hh"		// to work with Playing_Field

class Paddle :  public Dynamic, public Drawable, public Solid, public cs225::Listener
>>>>>>> master
{
public:
    /// Spawn a paddle at some given 2D coordinates
    Paddle( huge::geometry::Point initial_pos );
    
    // update the paddle state every game tick
    void update( float dt );

    // draw the paddle
    void draw() const;

    // collision-checking specialization
    bool check_collision( const Solid& other ) const;

<<<<<<< HEAD
=======
	virtual void handle_event(const cs225::Event & event_given);

>>>>>>> master
    huge::geometry::Point  position;   // player coordinates
    huge::collision::AABB  box;        // bounding box to check for collisions
    float                  velocity;   // moving velocity in the vertical axis

}; // Paddle
