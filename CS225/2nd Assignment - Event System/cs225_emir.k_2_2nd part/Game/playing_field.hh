#pragma once 

#include "huge.hh"
#include "common_interfaces.hh"

struct PlayingField : public Drawable, public Solid
{
    PlayingField();
    
    void draw() const;

    bool check_collision( const Solid& other ) const;


    huge::collision::AABB upper_wall;
    huge::collision::AABB lower_wall;

    huge::collision::AABB left_goal;
    huge::collision::AABB right_goal;

}; // PlayingField

class Score : public Drawable
{
public:
    Score();
    
    void draw() const;
    void increase();
	void decrease();

private:
    huge::graphics::Text displayed_score;
    unsigned int value;

}; // Score
