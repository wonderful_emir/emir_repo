/*! Common interface class declarations for game entities.
 * 
 *  Game objects that implement these interfaces will simulate belonging to 
 *  some game system (graphics, physics, etc.). These methods will be invoked
 *  automatically by the game itself.
 */
 
#pragma once

/// Classes that implement this interface can be drawn by the game
struct Drawable
{
    virtual void draw() const = 0;
};

/// Classes that implement this interface can be updated (run custom code) on every game tick
struct Dynamic 
{
    virtual void update( float dt ) = 0;
};

/// Classes that implement this interface can collide with other solids
struct Solid
{
    // Will return whether it's colliding with 'other'
    virtual bool check_collision( const Solid & other ) const = 0;
};
