/**********************************************************************************/
/*!
\file   	type_info.hh
\language 	C++
\author 	Emir Khanseitov
\par    	email: emir.k@digipen.edu
\par    	digiPen login: emir.k
\par    	Course: CS225
\par    	Assignment #2 - Event System
\date   	13/10/2018
\updated 	16/10/2018
\brief  
    This header contains declarations of functions that are used within TypeInfo 
    class as well as some templated functions that return TypeInfo class
        

\par Hours spent on this assignment: 
    8


\par Specific portions that gave you the most trouble: 
    - the fact that std::maps needed operator< overload
    - that we didn't need to build our own typeid() operator
    - I kind of forgot at one point how to turn a variable to a pointer... by &
    - Logic about Events has still left me wondering how it works
    - The way Multimap works, I know that my method may be not the most optimized
      when I loop
  
*/
/**********************************************************************************/
#pragma once

#include <typeinfo>

namespace cs225 {	

	class TypeInfo
	{
	public:

/*!****************************************************************************
 \fn	 ~TypeInfo()
 
 \brief	 Virtual destructor, means its Derivable

 \param  void
 
 \return void
******************************************************************************/
		virtual ~TypeInfo() {}

/*!****************************************************************************
 \fn	 TypeInfo(T& diff_object)
 
 \brief	 Templated non_default constructor

 \param  diff_object
T&
 
 \return void
******************************************************************************/
		template <typename T>
		TypeInfo(T& diff_object) : m_typeInfo(typeid(diff_object)) {}

/*!****************************************************************************
 \fn	 TypeInfo(const TypeInfo rhs)
 
 \brief	 Conversion Constructor

 \param  rhs
TypeInfo &
 
 \return void
******************************************************************************/
		TypeInfo(const std::type_info& rhs);

/*!****************************************************************************
 \fn	 operator==(const TypeInfo& rhs) const
 
 \brief	 Operator overload of ==, which means it's Comparable

 \param  rhs
 const TypeInfo&
 
 \return bool
******************************************************************************/
		bool operator==(const TypeInfo& rhs) const;

/*!****************************************************************************
 \fn	 operator!=(const TypeInfo& rhs) const
 
 \brief	 Operator overload of !=, which means it's Comparable

 \param  rhs
 const TypeInfo&
 
 \return bool
******************************************************************************/
		bool operator!=(const TypeInfo& rhs) const;

/*!****************************************************************************
 \fn	 get_name() const
 
 \brief	 Function getter that returns const char * - 'string'

 \param  void
 
 \return const char*
******************************************************************************/
		const char* get_name() const;

/*!****************************************************************************
 \fn	 operator<(const TypeInfo & rhs) const
 
 \brief	 Operator overload for less than <, for std::map

 \param  rhs
 const TypeInfo &
 
 \return bool
******************************************************************************/
		bool operator<(const TypeInfo & rhs) const;

	private:
		const std::type_info & m_typeInfo;

	}; // Class TypeInfo


/*!****************************************************************************
 \fn	 type_of(T& diff_obj)
 
 \brief	 Templated function to get type_of without outside TypeInfo class

 \param  diff_obj
 T&
 
 \return const TypeInfo
******************************************************************************/
	template <typename T>
	const TypeInfo type_of(const T& diff_obj) { return TypeInfo(typeid(diff_obj)); }

/*!****************************************************************************
 \fn	 type_of()
 
 \brief	 Templated function to get type_of without parameters

 \param  void
 
 \return const TypeInfo
******************************************************************************/
	template <typename T>
	const TypeInfo type_of() { return TypeInfo(typeid(T)); }


} // namespace cs225

