/**********************************************************************************/
/*!
\file   	event.hh
\language 	C++
\author 	Emir Khanseitov
\par    	email: emir.k@digipen.edu
\par    	digiPen login: emir.k
\par    	Course: CS225
\par    	Assignment #2 - Event System
\date   	13/10/2018
\updated 	18/10/2018
\brief  
    This header contains several classes that somewhat handle events. Some are 
    abstract classes, while others are more concrete
        

\par Hours spent on this assignment: 
    8


\par Specific portions that gave you the most trouble: 
    - the fact that std::maps needed operator< overload
    - that we didn't need to build our own typeid() operator
    - I kind of forgot at one point how to turn a variable to a pointer... by &
    - Logic about Events has still left me wondering how it works
    - The way Multimap works, I know that my method may be not the most optimized
      when I loop
  
*/
/**********************************************************************************/
#pragma once

#include <map>
#include "type_info.hh"

namespace cs225 {

	// Event (base) CLASS
	class Event
	{
	public:
/*!****************************************************************************
 \fn	 ~Event()
 
 \brief	 Virtual destructor to make the class derivable from

 \param  void
 
 \return void
******************************************************************************/
		virtual ~Event() {}
	};




	// HandlerFunction (base) CLASS
	class HandlerFunction
	{
	public:

/*!****************************************************************************
 \fn	 ~HandlerFunction()
 
 \brief	 Virtual destructor to make the class derivable from

 \param  void
 
 \return void
******************************************************************************/
		virtual ~HandlerFunction() {}

/*!****************************************************************************
 \fn	 handle(const Event & diff_event)
 
 \brief	 Function that calls Call() func, usually being invoked by an event

 \param  diff_event
 const Event &
 
 \return void
******************************************************************************/
		void handle(const Event & diff_event);
	
	private:
/*!****************************************************************************
 \fn	 call(const Event & event) = 0
 
 \brief	 Pure virtual function of call that invokes the handler

 \param  event
 const Event &
 
 \return void
******************************************************************************/
		virtual void call(const Event & event) = 0;
	};




	// TEMPLATED MemberFunctionHandler CLASS
	template <typename T, typename EVENT>
	class MemberFunctionHandler : public HandlerFunction
	{
	public:

		// MemberFunction is a pointer to a member func in T that takes an event
		typedef void (T::*MemberFunction)(const EVENT &);

/*!****************************************************************************
 \fn	 MemberFunctionHandler(T * obj, MemberFunction fn)
 
 \brief	 Non-default Templated Constructor for MemFuncHandler

 \param  obj
 T*

 \param	 fn
 MemberFunction
 
 \return void
******************************************************************************/
		MemberFunctionHandler(T * obj, MemberFunction fn) 
							: instance( obj ), function ( fn ) {}

/*!****************************************************************************
 \fn	 call( const Event & event)
 
 \brief	 Will be called by Handle function, which reacts to the event

 \param  event
 const Event &
 
 \return void
******************************************************************************/
		virtual void call( const Event & event)
		{
			(instance->*function)( static_cast<const EVENT &> (event) );
		}

	private:
		T * instance; 		  // The Who part of who.how(what);
		MemberFunction function; // The How part 
					// The What part -> is the EVENT
	};




	// EventHandler CLASS
	class EventHandler
	{
	public:

/*!****************************************************************************
 \fn	 ~HandlerFunction()
 
 \brief	 Destructor for no memory leaks

 \param  void
 
 \return void
******************************************************************************/
		~EventHandler();

/*!****************************************************************************
 \fn	 register_handler(T & obj, void (T::*fn)(const EVENT &))
 
 \brief	 Function that applies a new handler into Map based on type name

 \param  obj
 T&

 \param	 fn
 void (T::*fn)(const EVENT &)
 
 \return void
******************************************************************************/
		template <typename T, typename EVENT>
		void register_handler(T & obj, void (T::*fn)(const EVENT &))
		{
			// Sanity check to see if there's a value of such key value
			if(handlers.find(typeid(EVENT).name()) == handlers.end())
				// Apply into map and in the right place a new handler
				handlers[typeid(EVENT).name()] = new MemberFunctionHandler<T, EVENT>(&obj, fn);
		}

/*!****************************************************************************
 \fn	 handle( const Event & event )
 
 \brief	 Given a base event, runs the associated member function

 \param  event
 const Event &
 
 \return void
******************************************************************************/
		void handle( const Event & event );


	private:
		// Container that puts all the members by alphabet
		std::map<const char *, HandlerFunction *> handlers;
	};



	//


} // namespace cs225