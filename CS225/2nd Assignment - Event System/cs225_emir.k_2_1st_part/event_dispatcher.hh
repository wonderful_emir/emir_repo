/**********************************************************************************/
/*!
\file   	event_dispatcher.hh
\language 	C++
\author 	Emir Khanseitov
\par    	email: emir.k@digipen.edu
\par    	digiPen login: emir.k
\par    	Course: CS225
\par    	Assignment #2 - Event System
\date   	13/10/2018
\updated 	18/10/2018
\brief  
    This file contains declarations of the functions that Event Dispatcher uses
    to create subscribers to events, as well as the notifications of events to 
    applicable subscribers
        

\par Hours spent on this assignment: 
    8


\par Specific portions that gave you the most trouble: 
    - the fact that std::maps needed operator< overload
    - that we didn't need to build our own typeid() operator
    - I kind of forgot at one point how to turn a variable to a pointer... by &
    - Logic about Events has still left me wondering how it works
    - The way Multimap works, I know that my method may be not the most optimized
      when I loop

  
*/
/**********************************************************************************/
#pragma once

#include <string>
#include "event.hh"

namespace cs225 {

	// LISTENER CLASS
	class Listener
	{
	public:
/*!****************************************************************************
 \fn	 handle_event(const Event & ) = 0
 
 \brief	 Abstract function that would handle event based on definition in child

 \param  ...
 const Event &
 
 \return void
******************************************************************************/
		virtual void handle_event(const Event & ) = 0;

/*!****************************************************************************
 \fn	 ~Listener()
 
 \brief	 Virtual destructor to make the class derivable from

 \param  void
 
 \return void
******************************************************************************/
		virtual ~Listener() {}
	};



	// EVENT_DISPATCHER CLASS
	class EventDispatcher
	{
	public:

/*!****************************************************************************
 \fn	 ~EventDispatcher()
 
 \brief	 Destructor to make the class derivable from

 \param  void
 
 \return void
******************************************************************************/
		~EventDispatcher() {}

/*!****************************************************************************
 \fn	 get_instance()
 
 \brief	 Function to get a single (and only single) instance, Meyers singleton

 \param  void
 
 \return static EventDispatcher &
******************************************************************************/
		static EventDispatcher & get_instance()
		{
			// Create a static instance of EventDispatcher
			static EventDispatcher instance;
	
			// And created or not return instance
			return instance;
		}

/*!****************************************************************************
 \fn	 subscribe(Listener & given_listener, const TypeInfo event_type)
 
 \brief	 Function to get a single (and only single) instance, Meyers singleton

 \param  given_listener
 Listener & 

 \param	 event_type
 const TypeInfo

 \return void
******************************************************************************/
		void subscribe(Listener & given_listener, const TypeInfo event_type);

/*!****************************************************************************
 \fn	 operator<< (std::ostream &os, EventDispatcher & my_disp)
 
 \brief	 friend Operator overload of << for appropriate output

 \param  os
 std::ostream &

 \param	 event_type
 EventDispatcher &

 \return std::ostream &
******************************************************************************/
		friend std::ostream & operator<< (std::ostream &os, EventDispatcher & my_disp);

/*!****************************************************************************
 \fn	 get_subscribers()
 
 \brief	 Function Getter for getting subscribers multimap

 \param  void

 \return std::multimap<const TypeInfo, Listener *> & 
******************************************************************************/
		std::multimap<const TypeInfo, Listener *> & get_subscribers();

/*!****************************************************************************
 \fn	 get_subscribers()
 
 \brief	 Function to manually clear all the contents of the map

 \param  void

 \return void
******************************************************************************/
		void clear();

/*!****************************************************************************
 \fn	 trigger_event()
 
 \brief	 Function that calls other things handling events

 \param  event_passed
 const Event &

 \return void
******************************************************************************/
		void trigger_event(const Event & event_passed);

/*!****************************************************************************
 \fn	 unsubscribe(Listener & given_listener, const TypeInfo event_type)
 
 \brief	 On unsubscription the event dispatcher removes the entry from the subs

 \param  given_listener
 Listener &

 \param  event_type
 const TypeInfo

 \return void
******************************************************************************/
		void unsubscribe(Listener & given_listener, const TypeInfo event_type);


	private:
		// Hide constructors in private so they cannot get accessed
		EventDispatcher() {}
		EventDispatcher(const EventDispatcher &) {}

		// A container where you put subscribers to any event
		std::multimap<const TypeInfo, Listener *> subscribers;

		
	};




/*!****************************************************************************
 \fn	 trigger_event(const Event & event_passed)
 
 \brief	 Proxy function to ease the syntax

 \param  event_passed
const Event & 

 \return void
******************************************************************************/
	void trigger_event(const Event & event_passed);

} // namespace cs225