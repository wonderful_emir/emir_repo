/**********************************************************************************/
/*!
\file   	event.cc
\language 	C++
\author 	Emir Khanseitov
\par    	email: emir.k@digipen.edu
\par    	digiPen login: emir.k
\par    	Course: CS225
\par    	Assignment #2 - Event System
\date   	13/10/2018
\updated 	18/10/2018
\brief  
    This header contains several classes that somewhat handle events. Some are 
    abstract classes, while others are more concrete
        

\par Hours spent on this assignment: 
    8


\par Specific portions that gave you the most trouble: 
    - the fact that std::maps needed operator< overload
    - that we didn't need to build our own typeid() operator
    - I kind of forgot at one point how to turn a variable to a pointer... by &
    - Logic about Events has still left me wondering how it works
    - The way Multimap works, I know that my method may be not the most optimized
      when I loop
  
*/
/**********************************************************************************/

#include "event.hh"

namespace cs225 {

	// Function that calls Call() func, usually being invoked by an event
	void HandlerFunction::handle(const Event & diff_event)
	{
		// Abstract func -> means it's dependant on obj that implements this func.
		call(diff_event);
	}





	// Destructor for EventHandler for no memory leaks
	EventHandler::~EventHandler()
		{
			// Create an iterator
			std::map<const char *, HandlerFunction *>::iterator it = handlers.begin();
			
			// Create a destructor that clears each handlers.clear();
			for(it = handlers.begin(); it != handlers.end(); ++it)
				delete it->second;

			// Get rid of the map
			handlers.clear();
		}

	// Given a base event, runs the associated member function
	void EventHandler::handle( const Event & event )
	{
		// Create an iterator
		std::map<const char *, HandlerFunction *>::iterator it;

		// Use it to find by name
		it = handlers.find( typeid(event).name() );

		// if it's not in the end handle thy event
		if( it != handlers.end() )
			it->second->handle( event );
	}

} // namespace cs225