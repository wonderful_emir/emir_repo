/**********************************************************************************/
/*!
\file   	event_dispatcher.cc
\language 	C++
\author 	Emir Khanseitov
\par    	email: emir.k@digipen.edu
\par    	digiPen login: emir.k
\par    	Course: CS225
\par    	Assignment #2 - Event System
\date   	13/10/2018
\updated 	18/10/2018
\brief  
    This file contains definitions of the functions that Event Dispatcher uses
    to create subscribers to events, as well as the notifications of events to 
    applicable subscribers
        

\par Hours spent on this assignment: 
    8


\par Specific portions that gave you the most trouble: 
    - the fact that std::maps needed operator< overload
    - that we didn't need to build our own typeid() operator
    - I kind of forgot at one point how to turn a variable to a pointer... by &
    - Logic about Events has still left me wondering how it works
    - The way Multimap works, I know that my method may be not the most optimized
      when I loop

  
*/
/**********************************************************************************/

#include "event_dispatcher.hh"

#include <iostream>

namespace cs225{

	// EVENT_DISPATCHER CLASS
	// Apply the given listener into the map by putting it with appropriate event_type
	void EventDispatcher::subscribe(Listener & given_listener, const TypeInfo event_type)
	{ get_subscribers().insert(std::pair<const TypeInfo, Listener *>(event_type, &given_listener)); }

	// Function Getter for getting subscribers multimap
	std::multimap<const TypeInfo, Listener *> & EventDispatcher::get_subscribers() { return subscribers; }

	// friend Operator overload of << for appropriate output of EventDispatcher
	std::ostream & operator<< (std::ostream &os, EventDispatcher & my_disp)
	{
		// Since this operator wants info about Event Dispatcher as a String,
		// we need to loop it until we went through each KEY - EVENT TYPE
		// While in appropriate event type -> you go through each LISTENER
		// and write info about them

		// Iterators for looping
		std::multimap<const TypeInfo, Listener *>::iterator it = my_disp.get_subscribers().begin();
		std::multimap<const TypeInfo, Listener *>::iterator end_it = my_disp.get_subscribers().end();

		// Iterator for secondary looping of listeners
		std::multimap<const TypeInfo, Listener *>::iterator listeners_it;

		// Loop through each EVENT TYPE and print info about event into os
		while(it != end_it)
		{
			os << "The event type " << it->first.get_name() << " has the following subscribers:\n";

			// Create a pair of iterators to get the equal_range of listeners in that key
			std::pair <std::multimap<const TypeInfo, Listener *>::iterator, 
									 std::multimap<const TypeInfo, Listener *>::iterator> ret;

			// Get equal range of one key for all the other listeners
			ret = my_disp.get_subscribers().equal_range(it->first);

			// Print actual Listeners that are inside one key, 
			// it++ happens here as multimap counts members with one key same as all members 
			for(listeners_it = ret.first; listeners_it != ret.second; ++listeners_it, ++it)
				os << "\tAn instance of type " << typeid(*it->second).name() <<"\n";		
		}

		// Finally return the output stringified
		return os;
	}


	// Function to manually clear all the contents of the map, if it is not empty
	void EventDispatcher::clear() { if(get_subscribers().empty() != true)	get_subscribers().clear(); }

	// Trigger function is supposed to trigger event in all the subscribers of such event
	void EventDispatcher::trigger_event(const Event & event_passed)
	{
		// Iterators for looping
		std::multimap<const TypeInfo, Listener *>::iterator it = get_subscribers().begin();
		std::multimap<const TypeInfo, Listener *>::iterator end_it = get_subscribers().end();

		// Iterator for secondary looping of listeners
		std::multimap<const TypeInfo, Listener *>::iterator listeners_it;

		// Loop through each EVENT TYPE, to find matching ones
		while(it != end_it)
		{
			// if EVENT TYPE is same as of that given
			if(it->first == typeid(event_passed))
			{
				// Create a pair of iterators to get the equal_range of listeners in that key
				std::pair <std::multimap<const TypeInfo, Listener *>::iterator, 
										 std::multimap<const TypeInfo, Listener *>::iterator> ret;
	
				// Get equal range of one key for all the other listeners
				ret = get_subscribers().equal_range(it->first);
	
				// Print actual Listeners that are inside one key, 
				// it++ happens here as multimap counts members with one key same as all members 
				for(listeners_it = ret.first; listeners_it != ret.second; ++listeners_it, ++it)
					(*it->second).handle_event(event_passed);
			}
			else
				++it;
		}
	}

	// On unsubscription the event dispatcher removes the entry from the subscribers
	void EventDispatcher::unsubscribe(Listener & given_listener, const TypeInfo event_type)
	{
		// Iterators for looping
		std::multimap<const TypeInfo, Listener *>::iterator it = get_subscribers().begin();
		std::multimap<const TypeInfo, Listener *>::iterator end_it = get_subscribers().end();

		// Iterator for secondary looping of listeners
		std::multimap<const TypeInfo, Listener *>::iterator listeners_it;

		// Loop through each EVENT TYPE, to find matching ones
		while(it != end_it)
		{
			// if EVENT TYPE is same as of that given
			if(it->first == event_type)
			{
				// Create a pair of iterators to get the equal_range of listeners in that key
				std::pair <std::multimap<const TypeInfo, Listener *>::iterator, 
										 std::multimap<const TypeInfo, Listener *>::iterator> ret;
	
				// Get equal range of one key for all the other listeners
				ret = get_subscribers().equal_range(it->first);
				
				// Print actual Listeners that are inside one key, 
				// it++ happens here as multimap counts members with one key same as all members 
				for(listeners_it = ret.first; listeners_it != ret.second; ++listeners_it, ++it)
				{
					// If the Listeners match, then erase it from map and return
					if(typeid(*it->second).name() == typeid(given_listener).name())
					{
						get_subscribers().erase(listeners_it);
						return;
					}
				}
			}
			// else increase the iterator
			else
				++it;
		}

	}

	// Proxy function to ease the syntax
	void trigger_event(const Event & event_passed) 
	{ EventDispatcher::get_instance().trigger_event(event_passed); }

} // namespace cs225